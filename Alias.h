#pragma once

#include <string>
#include <memory>
#include <vector>
#include <boost/filesystem.hpp>

// Alias to of some std classes to match naming conventions

typedef std::string String;

template<typename T, std::size_t Size>
using ArrayFixed = std::array<T, Size>;

template<typename T>
using ArrayVector = std::vector<T>;

typedef boost::filesystem::path Path;

template<typename T>
using SharedPointer = std::shared_ptr<T>;

template<typename T>
using WeakPointer = std::weak_ptr<T>;
