#pragma once

#include <typeinfo>
#include <string>

struct AnyPointer {
    void *pointer = nullptr;
    std::type_info *type = nullptr; // pointer, not reference, so this struct can be copied

    template<typename T>
    static const AnyPointer Create(T* pointer)
    {
        AnyPointer val;
        val.pointer = pointer;
        val.type = &typeid(T);
        return val;
    }

    template<typename T>
    T* Cast()
    {
        if (!Typeof<T>())
            return nullptr;

        return pointer;
    }

    template<typename T>
    bool Typeof()
    {
        return typeid(T).hash_code() == type->hash_code();
    }

    bool IsNull()
    {
        return pointer == nullptr;
    }

    bool IsValid()
    {
        return pointer != nullptr && type != nullptr;
    }
};
