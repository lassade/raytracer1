#pragma once

//#include <memory>
#include <cstring>
#include <iterator>
#include <utility>

// basic Array implementation, improve peformance from std::vector
// TODO test if works with c++11 std::shared_ptr and std::unique_ptr
template<typename T>
class Array {
public:
	typedef T* Iterator;

private:
	unsigned size, count;
	T *data;

public:
	Array()
	{
		size = 4;
		count = 0;
		data = new T[size];
	}
	Array(std::initializer_list<T> list)
	{
		size = ((unsigned)list.size()) << 2;
		count = 0;
		data = new T[size];
		for (auto &item : list) data[count++] = item;
	}

	T& operator [](const unsigned i) {
		return data[i];
	}
	const T& operator [](const unsigned i) const {
		return data[i];
	}
	
	Array<T>& operator = (const Array<T> &other)
	{
		unsigned i;
		if (size < other.size) {
			size = other.size << 2;
			Rallocate();
		}
		for (i = 0; i < other.count; ++i)
			data[i] = std::move(other.arr[i]);
		count = other.count;
		return *this;
	}
	Array<T>& operator = (Array<T> &&other)
	{
		unsigned i;
		if (size < other.size) {
			size = other.size << 2;
			Rallocate();
		}
		for (i = 0; i < other.count; ++i)
			data[i] = std::move(other.arr[i]);
		count = other.count;
		return *this;
	}
	Array<T>& operator = (std::initializer_list<T> list)
	{
		unsigned lsize = (unsigned)list.size();
		if (size < lsize) {
			size = lsize << 2;
			Reallocate();
		}
		count = 0;
		for (auto &item : list) data[count++] = item;
		return *this;
	}

	void Append(const T &value)
	{
		if (size == count)
		{
			size <<= 2;
			Reallocate();
		}

		data[count++] = value;
	}
	void Append(T &&value)
	{
		if (size == count)
		{
			size <<= 2;
			Reallocate();
		}

		data[count++] = value;
    }
    bool Remove(const T &value)
    {
        for (unsigned i = 0; i < count; ++i)
        {
            if (data[i] == value)
            {
                RemoveIndex(i);
                return true;
            }
        }
        return false;
    }
    void RemoveIndex(const unsigned index)
    {
        if (index < 0) return;

        for (unsigned i = index + 1; i < count; ++i)
        {
            data[i - 1] = data[i];
        }
        count -= 1;
    }

    // TODO add Insert and Trim

    unsigned Count() { return count; }
    void Clear()
    {
        size = 4;
        count = 0;
        Reallocate();
    }
    void ClearNoRealloc()
    {
        count = 0;
    }

    void Reallocate(unsigned size)
    {
        this->size = size;
        Reallocate();
    }

	// upper case 
	Iterator Begin() { return data; }
	Iterator End() { return data + count; }

	// needed by foreach loop in c++11 and beyond
	Iterator begin() { return data; }
	Iterator end() { return data + count; }

	~Array()
	{
		delete[] data;
	}

private:
    inline void Reallocate()
    {
        T *vector = new T[size];
        std::memcpy(vector, data, count * sizeof(T));
        delete[] data;
        data = vector;
    }
};
