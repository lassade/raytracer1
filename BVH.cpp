
#include <limits>
#include "BVH.h"
#include "Raycaster.h"
#include "Util.h"

OptimizedRay::OptimizedRay(const Ray &ray)
    : origin(ray.origin),
      //inverted({ 1.0 / ray.direction(0), 1.0 / ray.direction(1), 1.0 / ray.direction(2) }),
      direction(ray.direction)
{
    for (int i = 0; i < 3; ++i) {
        inverted(i) = 1.0 / direction(i);
    }
}

BoundingBox::BoundingBox()
{
}

BoundingBox::BoundingBox(const Vec3 &min, const Vec3 &max)
    : min(min), max(max)
{
}

unsigned BoundingBox::LongestAxis()
{
    Vec3 size = max - min;
    if (size(0) > size(1))
    {
        if (size(0) > size(2)) return 0;
        else return 2;
    }
    else
    {
        if (size(1) > size(2))  return 1;
        else return 2;
    }
}

void BoundingBox::Expand(const Vec3 &point)
{
    for (int i = 0; i < 3; ++i)
    {
        max(i) = Max(point(i), max(i));
        min(i) = Min(point(i), min(i));
    }
}

// source code originated from https://tavianator.com/cgit/dimension.git/tree/libdimension/bvh/bvh.c
bool BoundingBox::Intersect(const OptimizedRay &optray, const double distance)
{
  // This is actually correct, even though it appears not to handle edge cases
  // (ray.n.{x,y,z} == 0).  It works because the infinities that result from
  // dividing by zero will still behave correctly in the comparisons.  Rays
  // which are parallel to an axis and outside the box will have tmin == inf
  // or tmax == -inf, while rays inside the box will have tmin and tmax
  // unchanged.

  double tx1 = (min(0) - optray.origin(0)) * optray.inverted(0);
  double tx2 = (max(0) - optray.origin(0)) * optray.inverted(0);

  double tmin = Min(tx1, tx2);
  double tmax = Max(tx1, tx2);

  double ty1 = (min(1) - optray.origin(1)) * optray.inverted(1);
  double ty2 = (max(1) - optray.origin(1)) * optray.inverted(1);

  tmin = Max(tmin, Min(ty1, ty2));
  tmax = Min(tmax, Max(ty1, ty2));

  double tz1 = (min(2) - optray.origin(2)) * optray.inverted(2);
  double tz2 = (max(2) - optray.origin(2)) * optray.inverted(2);

  tmin = Max(tmin, Min(tz1, tz2));
  tmax = Min(tmax, Max(tz1, tz2));

  return (tmax >= Max(0.0, tmin)) && (tmin < distance);
}

bool BoundingBox::Intersect(const OptimizedRay &optray)
{
    return Intersect(optray, std::numeric_limits<double>::infinity());
}

BoundingBox BoundingBox::BuildFromExtents(const Vec3 &offset, const Vec3 &extents)
{
    return BoundingBox(offset - extents, offset + extents);
}

// TODO find a faster algorithm
bool KDNode::Triangle::Raycast(const Ray &ray, Impact &impact, const double distance, const double bias = 0)
{
    Vec3 A0, A1, A2, R, D0, D1;

    do {
        Vec3 v = *vertices(0);
        A0 = v - *vertices(1);
        A1 = v - *vertices(2);
        A2 = ray.direction;
        R = v - ray.origin;
    } while(0);

#if 0
    D0 = {
        (A1(1) * A2(2)) - (A1(2) * A2(1)),
        (A1(2) * A2(0)) - (A1(0) * A2(2)),
        (A1(0) * A2(1)) - (A1(1) * A2(0))
    };

    double M = 1.0 / Vec3::Dot(A0, D0);

    D1 = {
        (A0(1) * R(2)) - (R(1) * A0(2)),
        (A0(2) * R(0)) - (R(2) * A0(0)),
        (A0(0) * R(1)) - (R(0) * A0(1))
    };
#else
// Simpler algorithm but is not faster than the last one
#define SAND3(var) var = (var + 1) % 3
    for (int i = 0, j = 1, k = 2; i < 3; ++i, SAND3(j), SAND3(k)) {
        D0(i) = A1(j) * A2(k) - (A1(k) * A2(j));
        D1(i) = A0(j) * R(k) - (A0(k) * R(j));
    }
#undef SAND3

    double M = 1.0 / Vec3::Dot(A0, D0);
#endif

    double t = -Vec3::Dot(A1, D1) * M;
    if (t < bias || t > distance) return false;

    double gamma = Vec3::Dot(A2, D1) * M;
    if (gamma < 0 || gamma > 1) return false;

    double beta = Vec3::Dot(R, D0) * M;
    if (beta < 0 || beta > (1 - gamma)) return false;

    Vec3 weights = { 1 - beta - gamma, beta, gamma };

    if (mask & NORMAL) // Have normals
    {
        Vec3 normal = Vec3::Zero();
        for (int i = 0; i < 3; ++i) {
            normal = normal + (weights(i) * (*normals(i)));
        }
        //normal.Normalize();
        impact.normal = normal;
    }
    else
    {
        // Default to hard edges
        Vec3 normal = A0.Cross(A1);
        //normal.Normalize();
        impact.normal = normal;
    }

    if (mask & UV) // Have uvs
    {
        Vec2 uv = Vec2::Zero();
        for (int i = 0; i < 3; ++i) {
            uv = uv + (weights(i) * (*uvs(i)));
        }
        impact.uv = uv;
    }
    else
    {
        impact.uv = Vec2::Zero();
    }

    impact.point = ray.Eval(t);
    impact.distance = t;
    impact.object = nullptr;

    return true;
}


KDNode::KDNode()
{

}

KDNode::~KDNode()
{
    if (right != nullptr) delete right;
    if (left != nullptr) delete left;
}

void KDNode::Split(int detph = 0)
{
    if (detph > 64) // quite shallow KDTree
        return;

    if (triangles.Count() <= 3) // should always less or equal to 1 at least
        return;

    auto left  = new KDNode();
    auto right = new KDNode();

    do
    {
        auto middle = Vec3::Zero();
        auto scalar = 1.0 / triangles.Count();
        Array<Vec3> centers; // triangles mid potins

        for(auto &t : triangles)
        {
            auto &v = t.vertices;
            auto center = Vec3::Zero();

            for (int i = 0; i < 3; ++i)
                center = center + (*v(i) * 0.3333333);

            middle = middle + (center * scalar);
            centers.Append(center);
        }

        auto axis = box.LongestAxis();
        for(int i = triangles.Count() - 1; i >= 0; --i)
        {
            if (middle(axis) >= centers[i](axis)) right->triangles.Append(triangles[i]);
            else left->triangles.Append(triangles[i]);
        }
    } while(0);

    triangles.Clear();

    // trim leafs
    if (right->triangles.Count() == 0)
    {
        delete right;
    }
    else
    {
        right->RebuildBoundingBox();
        right->Split(detph + 1);
        this->right = right;
    }

    if (left->triangles.Count() == 0)
    {
        delete left;
    }
    else
    {
        left->RebuildBoundingBox();
        left->Split(detph + 1);
        this->left = left;
    }
}

void KDNode::RebuildBoundingBox()
{
    box.max = box.min = *triangles[0].vertices(0);
    for(auto &t : triangles)
    {
        auto &v = t.vertices;
        for (int i = 0; i < 3; ++i)
            box.Expand(*v(i));
    }
}

bool KDNode::Raycast(const OptimizedRay &optray, Impact &impact, double &distance, const double bias = 0)
{
    if (!box.Intersect(optray, distance))
        return false;

    bool rhit = false, lhit = false;
    bool rvalid = right != nullptr, lvalid = left != nullptr;

    if (rvalid || lvalid)
    {
        rhit = (rvalid && right->Raycast(optray, impact, distance, bias));
        lhit = (lvalid && left->Raycast(optray, impact, distance, bias));
        return rhit || lhit;
    }

    bool hit = false;
    Ray ray(optray.origin, optray.direction);

    //impact.distance = std::numeric_limits<double>::max();
    for(auto &t : triangles)
    {
        Impact p;
        if (t.Raycast(ray, p, distance, bias))
        {
            if (distance > p.distance)
            {
                hit = true;
                impact = p;
                distance = p.distance; // little trick, change distance for all Raycasts functions not just this one
            }
        }
    }

    return hit;
}

KDNode *KDNode::Build(const Mesh &mesh)
{
    KDNode *node = new KDNode();
    node->box.max = node->box.min = mesh.verticies[0];

    // build the triangle array and bound box
    unsigned index = 0;
    for(Mesh::TriangleIndex &triangleIndex : mesh.triangles)
    {
        Triangle triangle;
        triangle.mask = Triangle::ALL;
        triangle.index = index++;
        for (int i = 0; i < 3; ++i)
        {
            Vec3 *vertex;
            Vec2 *uv;
            Vec3 *normal;

#define GetPointerOrDefault(Target, Array, Comp, Mask, Default) {\
    int k = triangleIndex.Comp(i);\
    if (k < 0) { Target = Default; triangle.mask &= ~Mask; }\
    else { Target = &mesh.Array[k]; } }

            GetPointerOrDefault(vertex, verticies, v, Triangle::VERTEX, nullptr);
            GetPointerOrDefault(uv, uvs, u, Triangle::UV, nullptr);
            GetPointerOrDefault(normal, normals, n, Triangle::NORMAL, nullptr);

#undef GetPointerOrDefault

            triangle.vertices(i) = vertex;
            triangle.normals(i) = normal;
            triangle.uvs(i) = uv;

            node->box.Expand(*vertex);
        }
        node->triangles.Append(triangle);
    }

    node->Split(0);
    return node;
}
