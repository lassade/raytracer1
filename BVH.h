#pragma once

#include "Vector.h"
#include "Array.h"

struct Ray;
struct Mesh;
struct Impact;


// Bounding Volume Hierarchy (BVH)


// A ray with pre-calculated reciprocals to avoid divisions.
struct OptimizedRay
{
  Vec3 origin;   // The origin of the ray.
  Vec3 inverted; // The inverse of each component of the ray's slope
  Vec3 direction;

  OptimizedRay(const Ray &ray);
};

struct BoundingBox
{
    Vec3 min;
    Vec3 max;

    BoundingBox();
    BoundingBox(const Vec3 &min, const Vec3 &max);

    unsigned LongestAxis();
    void Expand(const Vec3 &point);

    // Ray-AABB intersection test, by the slab method. Highly optimized.
    bool Intersect(const OptimizedRay &optray, const double distance);

    bool Intersect(const OptimizedRay &optray);

    static BoundingBox BuildFromExtents(const Vec3 &offset, const Vec3 &extents);
};

class KDNode
{
private:
    struct Triangle
    {
        static const unsigned ALL = -1;
        static const unsigned VERTEX = 1;
        static const unsigned UV = 2;
        static const unsigned NORMAL = 4;

        unsigned index = -1;
        unsigned mask = ALL;
        Vec<Vec3*, 3> vertices;
        Vec<Vec3*, 3> normals;
        Vec<Vec2*, 3> uvs;

        bool Raycast(const Ray &ray, Impact &impact, const double distance, const double bias = 0);
    };

    BoundingBox box;
    KDNode *left = nullptr, *right = nullptr;

    // TODO maybe will be better if this was a stripe like in a triangle strip, for really large meshes
    Array<Triangle> triangles;

    KDNode();
    void Split(int detph);
    void RebuildBoundingBox();

public:
    ~KDNode();

    bool Raycast(const OptimizedRay &optray, Impact &impact, double &distance, const double bias = 0);
    static KDNode* Build(const Mesh &mesh);
};

