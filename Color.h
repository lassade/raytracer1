#pragma once

#include "Vector.h"

typedef Vec<double, 3> RGB;
typedef Vec<double, 4> ARGB; // Not very suitable
typedef Vec<double, 4> RGBA;

typedef Vec<float, 3> RGBF;
typedef Vec<float, 4> ARGBF;
typedef Vec<float, 4> RGBAF;

typedef Vec<unsigned char, 3> RGB24;
typedef Vec<unsigned char, 4> ARGB32;
typedef Vec<unsigned char, 4> RGBA32;
