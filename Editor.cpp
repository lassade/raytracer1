#include <iostream>
#include <future>

// ImGui and OpenGL headers
#include <imgui.h>
#define IMGUI_DEFINE_MATH_OPERATORS
#define IMGUI_DEFINE_PLACEMENT_NEW
#include <imgui_internal.h>
#include "imgui_impl_glfw_gl3.h"

#include <GL/gl3w.h>
#include <GLFW/glfw3.h>

#include "Editor.h"
#include "AnyPointer.h"

// Extras
namespace ImGui
{
    bool ButtonEx(const char* label, const char* customId, const ImVec2& size_arg, ImGuiButtonFlags flags)
    {
        ImGuiWindow* window = GetCurrentWindow();
        if (window->SkipItems)
            return false;

        ImGuiContext& g = *GImGui;
        const ImGuiStyle& style = g.Style;
        const ImGuiID id = window->GetID(customId);
        const ImVec2 label_size = CalcTextSize(label, NULL, true);

        ImVec2 pos = window->DC.CursorPos;
        if ((flags & ImGuiButtonFlags_AlignTextBaseLine) && style.FramePadding.y < window->DC.CurrentLineTextBaseOffset) // Try to vertically align buttons that are smaller/have no padding so that text baseline matches (bit hacky, since it shouldn't be a flag)
            pos.y += window->DC.CurrentLineTextBaseOffset - style.FramePadding.y;
        ImVec2 size = CalcItemSize(size_arg, label_size.x + style.FramePadding.x * 2.0f, label_size.y + style.FramePadding.y * 2.0f);

        const ImRect bb(pos, pos + size);
        ItemSize(bb, style.FramePadding.y);
        if (!ItemAdd(bb, id))
            return false;

        if (window->DC.ItemFlags & ImGuiItemFlags_ButtonRepeat) flags |= ImGuiButtonFlags_Repeat;
        bool hovered, held;
        bool pressed = ButtonBehavior(bb, id, &hovered, &held, flags);

        // Render
        const ImU32 col = GetColorU32((hovered && held) ? ImGuiCol_ButtonActive : hovered ? ImGuiCol_ButtonHovered : ImGuiCol_Button);
        RenderFrame(bb.Min, bb.Max, col, true, style.FrameRounding);
        RenderTextClipped(bb.Min + style.FramePadding, bb.Max - style.FramePadding, label, NULL, &label_size, style.ButtonTextAlign, &bb);

        // Automatically close popups
        //if (pressed && !(flags & ImGuiButtonFlags_DontClosePopups) && (window->Flags & ImGuiWindowFlags_Popup))
        //    CloseCurrentPopup();

        return pressed;
    }

    bool Button(const char* label, const char* id, const ImVec2& size_arg = ImVec2(0,0))
    {
        return ButtonEx(label, id, size_arg, 0);
    }

    bool InputDouble(const char *name, double &number)
    {
        bool changed = false;
        float floatNumber = (float)number;

        //ImGui::PushItemWidth(ImGui::GetContentRegionAvailWidth() * 0.5f);
        if (ImGui::DragFloat(name, &floatNumber))
        {
            number = floatNumber;
            changed =  true;
        }
        //ImGui::PopItemWidth();

        return changed;
    }

    bool SliderDouble(const char *name, double &number, double min, double max)
    {
        bool changed = false;
        float floatNumber = (float)number;

        //ImGui::PushItemWidth(ImGui::GetContentRegionAvailWidth() * 0.5f);
        if (ImGui::SliderFloat(name, &floatNumber, min, max))
        {
            number = floatNumber;
            changed =  true;
        }
        //ImGui::PopItemWidth();

        return changed;
    }

    template<unsigned N>
    bool InputVec(const char *name, Vec<double, N> &vector)
    {
        float array[N]; // tempory array to use in the input fields

        vector.Copy(array);
        if (ImGui::DragFloatN(name, array, N, 1.0f, 0.0f, 0.0f, "%.03f", 1.0f))
        {
            vector.Set(array);
            return true;
        }

        return false;
    }

    bool InputVec2(const char *name, Vec2 &vector)
    {
        return InputVec<2>(name, vector);
    }

    bool InputVec3(const char *name, Vec3 &vector)
    {
        return InputVec<3>(name, vector);
    }

    bool InputRGB(const char *name, RGB &color)
    {
        float array[3]; // tempory array to use in the input fields

        color.Get(array);
        if (ImGui::ColorEdit3(name, array))
        {
            color.Set(array);
            return true;
        }

        return false;
    }

    bool InputString(const char* label, String &string)
    {
        ArrayFixed<char, 128> buff;
        std::copy(string.begin(), string.end(), buff.data());
        if (ImGui::InputText(label, buff.data(), 128))
        {
            string = String(buff.data());
            return true;
        }
        return false;
    }

    void Tooltip(const char* desc)
    {
        //ImGui::TextDisabled("(?)");
        if (ImGui::IsItemHovered())
        {
            ImGui::BeginTooltip();
            ImGui::PushTextWrapPos(450.0f);
            ImGui::TextUnformatted(desc);
            ImGui::PopTextWrapPos();
            ImGui::EndTooltip();
        }
    }

    void ShowHelpMarker(const char* desc)
    {
        ImGui::TextDisabled("(?)");
        Tooltip(desc);
    }

    // Image snippets
    void Image(const GUITexture* tex)
    {
       Image((void*)(intptr_t)tex->id, ImVec2(tex->width, tex->height));
    }

    // Margins order is upper, lower, right, left
    void ImageFitAvail(ImTextureID texture, ImVec2 textureSize, ImVec4 margins = ImVec4(0, 0, 0, 0))
    {
        auto w = (float) textureSize.x;
        auto h = (float) textureSize.y;
        auto aspect = w / h;

        auto avail = ImGui::GetContentRegionAvail();
        avail.x -= margins.z + margins.w;
        avail.y -= margins.x + margins.y;
        auto size = avail;
        auto indent = false;
        auto space = false;

        // Fit in screen
        if (w > h)
        {
            auto x = size.y * aspect;
            if(size.x < x)
            {
                size.y = size.x / aspect;
                space = true;
            }
            else
            {
                size.x = x;
                indent = true;
            }
        }
        else
        {
            auto y = size.x / aspect;
            if(size.y > y)
            {
                size.y = y;
                space = true;
            }
            else
            {
                size.x = size.y * aspect;
                indent = true;
            }
        }

        if (size.y > 0 && size.x > 0)
        {
            double margin = 0;
            if (indent)
            {
                margin = Max(avail.x - size.x, 0.0f) * 0.5;
                ImGui::Indent(margin);
            }

            if (space)
            {
                margin = Max(avail.y - size.y, 0.0f) * 0.5;
                ImGui::ItemSize(ImVec2(0, margin));
            }

            ImGui::Image(texture, size);

            if (indent)
                ImGui::Unindent(margin);
        }
    }

    void ImageFitAvail(GUITexture* tex, ImVec4 margins = ImVec4(0, 0, 0, 0))
    {
       ImageFitAvail((void*)(intptr_t)tex->id, ImVec2(tex->width, tex->height), margins);
    }
}



GUITexture::GUITexture()
{
    glGenTextures(1, &id);
}

GUITexture::~GUITexture()
{
    glDeleteTextures(1, &id);
}

void GUITexture::Copy(void *data)
{
    GLint last_texture;
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
    //glGenTextures(1, &id);
    glBindTexture(GL_TEXTURE_2D, id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

    // Restore state
    glBindTexture(GL_TEXTURE_2D, last_texture);
}

void GUITexture::Copy(void *data, int width, int height)
{
    this->width = width;
    this->height = height;
    Copy(data);
}

// Holds main selection attributes
class Selection
{
public:
    static Array<AnyPointer> selectionArray;

    static AnyPointer Frist()
    {
        if (selectionArray.Count() > 0)
            return selectionArray[0];

        return AnyPointer();
    }

    static int Count()
    {
        return selectionArray.Count();
    }

    template<class T>
    static bool IsSelected(T *obj)
    {
        auto any = AnyPointer::Create<T>(obj);
        for(auto &sel : selectionArray)
            if (sel.pointer == any.pointer && sel.type == any.type)
            {
                return true;
            }

        return false;
    }

    template<class T>
    static void Append(T *obj)
    {
        auto any = AnyPointer::Create<T>(obj);
        selectionArray.Append(any);
    }

    static void AppendRaw(AnyPointer ptr)
    {
        selectionArray.Append(ptr);
    }

    template<class T>
    static void Select(T *obj)
    {
        Clear();
        Append<T>(obj);
    }

    static void SelectRaw(AnyPointer ptr)
    {
        Clear();
        AppendRaw(ptr);
    }

    static void Clear()
    {
        selectionArray.Clear();
    }
};

class Editor
{
private:
    static bool dirty;
protected:
    EditorProgram *mainProgram;

public:
    char *name;

    Editor(EditorProgram *mainProgram, const char *name)
        : mainProgram(mainProgram), name(name)
    {
    }

    virtual void OnGUI()
    {

    }

    // Helper funtions
    inline const EditorWindow* GetEditorWindowByName(char* name)
    {
        return (EditorWindow*)mainProgram->GetEditorByName(name);
    }
    inline const Render* GetRender()
    {
        return mainProgram->GetRender();
    }
    inline static void FlagDirty(bool status)
    {
        dirty = dirty || status;
    }
    inline static void MarkDirty()
    {
        dirty = true;
    }
    inline static bool IsDirty()
    {
        return dirty;
    }
    inline static void CleanDirty()
    {
        dirty = false;
    }

    static void ImportModelObj(Scene *scene)
    {
        auto path = OpenFileDialog("*.obj");
        if (path.size() <= 0) return;

        Mesh *mesh = new Mesh();
        if (Mesh::LoadFromObj(path.c_str(), *mesh))
        {
            scene->objects.Append(mesh);
            MarkDirty();
        }
        else
        {
            // TODO show modal could not load obj
            delete mesh;
        }
    }

    static void ImportTexture(Scene *scene)
    {
        auto path = OpenFileDialog("*.png *.jpg *.tif *.bmp");
        if (path.size() <= 0) return;

        Texture2D *texture = nullptr;
        if (Texture2D::LoadFromFile(texture, path, true))
        {
            scene->textures.Append(texture);
        }
    }

    static void DeleteSelectedObjects(Scene *scene)
    {
        bool changed = false;
        for(auto &selected : Selection::selectionArray)
        {
            auto placeable = selected.Cast<Placeable>();
            if (placeable == nullptr) continue;

            auto light = dynamic_cast<Light*>(placeable);
            if (light == nullptr)
            {
                changed |= scene->objects.Remove(placeable);
            }
            else
            {
                changed |= scene->lights.Remove(light);
            }
        }
        FlagDirty(changed);
    }

    static String Exec(const char* cmd)
    {
        ArrayFixed<char, 128> buffer;
        String result;
        SharedPointer<FILE> pipe(popen(cmd, "r"), pclose);
        if (!pipe) return nullptr; //throw std::runtime_error("popen() failed!");
        while (!feof(pipe.get())) {
            if (fgets(buffer.data(), 128, pipe.get()) != nullptr)
                result += buffer.data();
        }
        return result;
    }

    // Separete multiple extensions using space
    static String OpenFileDialog(const char* filter = nullptr)
    {
        String cmd = "kdialog --getopenfilename `pwd`";
        if (filter)
        {
            cmd +=  " \'";
            cmd +=  filter;
            cmd +=  "\'";
        }
        auto path = Exec(cmd.c_str());
        if (path.size() > 0)
            path.erase(path.size() - 1);

        return path;
    }

    // Separete multiple extensions using space
    static String SaveFileDialog(const char* filter = nullptr)
    {
        String cmd = "kdialog --getsavefilename `pwd`";
        if (filter)
        {
            cmd +=  " \'";
            cmd +=  filter;
            cmd +=  "\'";
        }
        auto path = Exec(cmd.c_str());
        if (path.size() > 0)
            path.erase(path.size() - 1);

        return path;
    }
};

bool Editor::dirty = true;

class EditorWindow : public Editor
{
public:
    bool open = false;
    float alpha = -1.0f;
    int flags = 0;
    int defalutWidth = 320, defalutHeigth = 240;
    int defalutX = 40, defalutY = 40;

    EditorWindow(EditorProgram *mainProgram, const char *name, const bool defaultOpen)
        : Editor(mainProgram, name), open(defaultOpen)
    {
    }

    virtual void OnWindowGUI() {}

    void OnGUI() override
    {
        if (!open) return;

        auto title = GetWindowTitle();
        if (ImGui::Begin(title, &open, ImVec2(defalutWidth, defalutHeigth), alpha, flags))
        {
            OnWindowGUI();
        }
        ImGui::End();
        ImGui::SetWindowPos(title, ImVec2(defalutX, defalutY), ImGuiCond_FirstUseEver);
    }

    void Open()
    {
        open = true;
    }

    void Close()
    {
        open = false;
    }

    virtual const char* GetWindowTitle()
    {
        //static char* title = "Window";
        return name;//title;
    }
};



Array<AnyPointer> Selection::selectionArray;

class MainEditor : public Editor
{
private:
    bool demoWindow = false;

public:
    using Editor::Editor;

    void OnGUI() override
    {
        // Toolbar
        if (ImGui::BeginMainMenuBar())
        {
            if (ImGui::BeginMenu("File"))
            {
                if (ImGui::MenuItem("New", "Ctrl+N", false, false)) { }
                if (ImGui::MenuItem("Open", "Ctrl+S", false, false)) { }
                if (ImGui::MenuItem("Save", "Ctrl+O", false, false)) { }
                if (ImGui::MenuItem("Quit", "Alt+F4")) { mainProgram->Close(); }
                ImGui::EndMenu();
            }
            if (ImGui::BeginMenu("Edit"))
            {
                if (ImGui::MenuItem("Undo", "CTRL+Z")) {}
                if (ImGui::MenuItem("Redo", "CTRL+Y", false, false)) {}  // Disabled item
                ImGui::Separator();
                if (ImGui::MenuItem("Cut", "CTRL+X")) {}
                if (ImGui::MenuItem("Copy", "CTRL+C")) {}
                if (ImGui::MenuItem("Paste", "CTRL+V")) {}
                ImGui::EndMenu();
            }
            if (ImGui::BeginMenu("Windows"))
            {
                #define OPEN_WINDOW_MENU_ITEM(WinName) {\
                    auto window = GetEditorWindowByName(WinName); \
                    if (ImGui::MenuItem(WinName, NULL, false, window != nullptr)) \
                        window->Open(); \
                    }

                OPEN_WINDOW_MENU_ITEM("Outliner");
                OPEN_WINDOW_MENU_ITEM("Inspector");
                OPEN_WINDOW_MENU_ITEM("Render");
                OPEN_WINDOW_MENU_ITEM("Asset Browser");
                OPEN_WINDOW_MENU_ITEM("Scene");

                #undef OPEN_WINDOW_MENU_ITEM
                ImGui::EndMenu();
            }
            if (ImGui::BeginMenu("Help"))
            {
                if (ImGui::MenuItem("ImGui Demo", NULL)) { demoWindow = true; }
                if (ImGui::MenuItem("About", NULL)) { GetEditorWindowByName("About")->Open(); }
                ImGui::EndMenu();
            }
            ImGui::EndMainMenuBar();
        }

        if (demoWindow)
        {
            ImGui::ShowTestWindow(&demoWindow);
        }
    }
};

// TODO
class AssetsWindow : public EditorWindow
{
    // Keep track of Textures and other assets that maybe used inside the editor
    // For now only textures !!!
    Array<GUITexture*> thumnails;

public:

    using EditorWindow::EditorWindow;

    void OnWindowGUI() override
    {
        if (ImGui::Button("Import Texture"))
        {
            Texture2D *texture = nullptr;

            auto path = OpenFileDialog("*.png *.jpg *.tif *.bmp");
            if (Texture2D::LoadFromFile(texture, path, true))
            {
                auto render = GetRender();
                render->scene->textures.Append(texture);

                // Create texture thumnail
                auto thumb = new GUITexture();
                auto i = (int)floor(texture->GetMipmapCount() * 0.5);
                auto size = texture->GetTextureSize(i);
                thumb->Copy(texture->GetTexturePointer(i), size(0), size(1));
                thumnails.Append(thumb);
            }
        }

        ImGui::Separator();

        // TODO Vertical Scroll

        for (auto thumb = thumnails.Begin(); thumb != thumnails.End(); ++thumb)
        {
            if (ImGui::ImageButton((void*)(intptr_t)(*thumb)->id, ImVec2(64,64), ImVec2(0,0), ImVec2(1, 1), -1, ImColor(0,0,0,255)))
            {

            }
            ImGui::SameLine();
        }

    }
};

// FIXME Changin properties of disabled objects makes the scene dirty
class InspectorWindow : public EditorWindow
{
private:
    AnyPointer inspectedObject;
    GUITexture *thumbnail = nullptr;
    bool objectChanged = false;

    template<typename T>
    bool Inspect(AnyPointer &target)
    {
        auto t = target.Cast<T>();
        if (t == nullptr) return false;
        Inspect (*t);
        return true;
    }

    template<typename T, typename Y>
    bool InspectDynamic(Y *target)
    {
        auto t = dynamic_cast<T*>(target);
        if (t == nullptr) return false;
        Inspect (*t);
        return true;
    }

    bool InspectorHeader(const char* title, const char* tooltip = nullptr)
    {
        //ImGui::Text(title);
        return ImGui::CollapsingHeader(title, ImGuiTreeNodeFlags_DefaultOpen | ImGuiTreeNodeFlags_CollapsingHeader);
    }

    void Inspect(Render &render)
    {
        if (InspectorHeader("Render Settings"))
        {
            ImGui::Text("Render Size");

            FlagDirty(ImGui::InputInt("Width", &render.width));
            render.width = Max(render.width, 0u);

            FlagDirty(ImGui::InputInt("Height", &render.height));
            render.height = Max(render.height, 0u);

            FlagDirty(ImGui::Combo("Render Type", &render.renderType, "Default\0Normals (Debug)\0UVs (Debug)\0\0"));
        }
    }

    void Inspect(Scene &scene)
    {
        if (InspectorHeader("World"))
        {
            FlagDirty(ImGui::InputRGB ("Ambient Color", scene.ambientColor));
        }
    }

    void Inspect(Transform &transform)
    {
        // Header
        if (InspectorHeader("Transform", "Use to transform the object in the world"))
        {
            FlagDirty(ImGui::InputVec3("Location", transform.location));
            FlagDirty(ImGui::InputVec3("Rotation", transform.rotation));
            FlagDirty(ImGui::InputVec3("Scale", transform.scale));
        }
    }

    void Inspect(Placeable &placeable)
    {
        //if (InspectorHeader("Common"))
        {
            ImGui::InputString("Name", placeable.name);
            FlagDirty(ImGui::Checkbox("Enabled", &(placeable.enabled)));
        }

        Inspect(placeable.transform);
        bool hasMaterial = true;
        while (true)
        {
            // subclasses
            auto p = &placeable;
            if (InspectDynamic<Light>(p)) { hasMaterial = false; break; }
            if (InspectDynamic<Mesh>(p)) break;
            if (InspectDynamic<Sphere>(p)) break;
            if (InspectDynamic<Plane>(p)) break;
            break;
        }
        if (hasMaterial)
            Inspect(placeable.material);
    }

    void Inspect(Camera &camera)
    {
        Inspect(camera.transform);

        if (InspectorHeader("Camera"))
        {
            FlagDirty(ImGui::InputDouble("Field of View", camera.fieldOfView));
            FlagDirty(ImGui::InputDouble("Focal Distance", camera.focalDistance));
            FlagDirty(ImGui::InputDouble("Exposure", camera.exposure));
        }
    }

    void Inspect(Light &light)
    {
        if (InspectorHeader("Light", "Light attributes"))
        {
            FlagDirty(ImGui::Combo("Type", &light.lightType, "Point\0Directional\0Spot\0Area\0\0"));
            FlagDirty(ImGui::InputRGB("Color", light.color));
            FlagDirty(ImGui::Checkbox("Cast Shadows", &light.castShadows));
            FlagDirty(ImGui::InputDouble("Intensity", light.intensity));
            FlagDirty(ImGui::InputInt("Samples", &(light.samplesCount)));

            if (light.lightType != Light::LT_DIRECTIONAL)
            {
                FlagDirty(ImGui::InputDouble("Radius", light.radius));
                FlagDirty(ImGui::InputDouble("Reach", light.reach));
                FlagDirty(ImGui::Combo("Decay Type", &(light.decayType), "Linear\0Exponential\0\0"));
            }
            else if (light.lightType == Light::LT_AREA)
            {
                FlagDirty(ImGui::InputVec2("Size", light.size));
            }
            else if (light.lightType == Light::LT_SPOT)
            {
                FlagDirty(ImGui::InputDouble("Inner View Cone", light.innerViewCone));
                FlagDirty(ImGui::InputDouble("Outer View Cone", light.outerViewCone));
            }
        }
    }

    void Inspect(Mesh &mesh)
    {
        if (InspectorHeader("Mesh"))
        {
            //ImGui::Text("None Editable Attributes");
            ImGui::Text("Verticies: %d", mesh.verticies.Count());
            ImGui::Text("Triangles: %d", mesh.triangles.Count() / 3);
        }
    }

    void Inspect(Sphere &sphere)
    {
        if (InspectorHeader("Sphere"))
        {
            FlagDirty(ImGui::InputDouble("Raidus", sphere.radius));
        }
    }

    void Inspect(Plane &plane)
    {
        if (InspectorHeader("Plane"))
        {
            ImGui::Text("None Editable Attributes");
        }
    }

    void Inspect(Material &material)
    {
        if (InspectorHeader("Material"))
        {
            if (material.type != Material::TYPE_GLASS)
            {
                FlagDirty(ImGui::Combo("Type", &material.type, "Lambert\0Phong\0Glass\0\0"));
                FlagDirty(ImGui::InputRGB("Diffuse Color", material.diffuseColor));
                FlagDirty(PickTexture("Diffuse Map", material.diffuseMap));
            }

            if (material.type == Material::TYPE_PHONG)
            {
                FlagDirty(ImGui::InputDouble("Specular Power", material.specularIntensity));
                FlagDirty(ImGui::SliderDouble("Reflection Scaler", material.reflectionIntensity, 0.0, 1.0));
                FlagDirty(ImGui::InputRGB("Specular Color", material.specularColor));
                FlagDirty(PickTexture("Specular Map", material.specularMap));
            }
        }
    }

    void Inspect(Texture2D &texture)
    {
        if (InspectorHeader("Texture"))
        {
            ImGui::InputString("Name", texture.name);
            FlagDirty(ImGui::Combo("Warp Mode", &texture.warpMode, "Repeat\0Clamp\0\0"));

            if (objectChanged)
            {
                if (thumbnail == nullptr)
                    thumbnail = new GUITexture();

                // TODO Don't load the full size image into the memory
                auto size = texture.GetTextureSize(0);
                thumbnail->Copy(texture.GetTexturePointer(0), size(0), size(1));
            }

            float tex_w = 240;//thumbnail->width;
            float tex_h = 240;//thumbnail->height;
            auto tex_id = (void*)(intptr_t)thumbnail->id;

            ImVec2 tex_screen_pos = ImGui::GetCursorScreenPos();
            ImGui::Image(tex_id, ImVec2(tex_w, tex_h));
            if (ImGui::IsItemHovered())
            {
                ImGui::BeginTooltip();
                float focus_sz = 32.0f;
                float focus_x = ImGui::GetMousePos().x - tex_screen_pos.x - focus_sz * 0.5f; if (focus_x < 0.0f) focus_x = 0.0f; else if (focus_x > tex_w - focus_sz) focus_x = tex_w - focus_sz;
                float focus_y = ImGui::GetMousePos().y - tex_screen_pos.y - focus_sz * 0.5f; if (focus_y < 0.0f) focus_y = 0.0f; else if (focus_y > tex_h - focus_sz) focus_y = tex_h - focus_sz;
//                ImGui::Text("Min: (%.2f, %.2f)", focus_x, focus_y);
//                ImGui::Text("Max: (%.2f, %.2f)", focus_x + focus_sz, focus_y + focus_sz);
                ImVec2 uv0 = ImVec2((focus_x) / tex_w, (focus_y) / tex_h);
                ImVec2 uv1 = ImVec2((focus_x + focus_sz) / tex_w, (focus_y + focus_sz) / tex_h);
                ImGui::Image(tex_id, ImVec2(128,128), uv0, uv1, ImColor(255,255,255,255), ImColor(255,255,255,128));
                ImGui::EndTooltip();
            }

            auto size = texture.GetTextureSize(0);
            ImGui::Separator();
            ImGui::TextWrapped("Texture Info\nWidth: %d\nHeight: %d", size(0), size(1));
        }
    }

    bool PickTexture(const char* title, Texture2D *&texture)
    {
        static String filter;
        bool changed = false;

        ImGui::LabelText(title, (texture == nullptr) ? "None" : texture->name.c_str());

        String id("Pick.");
        id.append(title);
        if (ImGui::Button("Pick", id.c_str()))
        {
            ImGui::OpenPopup(title);
        }

        if (ImGui::BeginPopup(title))
        {
            ImGui::InputString("Filter", filter);
            ImGui::Separator();

            if (ImGui::Selectable("None", texture == nullptr))
            {
                texture = nullptr;
            }

            for(Texture2D *tex : GetRender()->scene->textures)
            {
                if (filter.size() > 0 && tex->name.find(filter) == std::string::npos) continue;
                auto selected = tex == texture;
                if (ImGui::Selectable(tex->name.c_str(), selected))
                {
                    texture = tex;
                    if (!selected) changed = true;
                }
            }

            ImGui::EndPopup();
        }

        return changed;
    }

public:

    InspectorWindow(EditorProgram *mainProgram, const char *name, const bool defaultOpen)
        : EditorWindow(mainProgram, name, defaultOpen)
    {
    }

    void OnWindowGUI() override
    {
        if(Selection::Count() > 1)
        {
            ImGui::TextWrapped("Multiple object edit is not supported yet");
            ImGui::SameLine();
            ImGui::ShowHelpMarker("Edit selected objects attributes");
            return;
        }
        else if (Selection::Count() == 0)
        {
            if (inspectedObject.IsNull())
                return;
        }

        auto selected = Selection::Frist();
        objectChanged = selected.pointer != inspectedObject.pointer; // Detect if an objct changed
        inspectedObject = selected;
        auto o = inspectedObject; // name alias
        while (true)
        {
            // base classes only
            if (Inspect<Placeable>(o)) break;
            if (Inspect<Camera>(o)) break;
            if (Inspect<Scene>(o)) break;
            if (Inspect<Render>(o)) break;
            if (Inspect<Texture2D>(o)) break;
            break;
        }
    }
};

class RendererViewWindow : public EditorWindow
{
    bool renderAvail = false;
    bool renderInProgress = false;
    double renderLastUpdate = 0;
    bool live = false;
    double liveUpdateInterval = 0.5;
    GUITexture *renderImage = nullptr;
    RGBA32 *renderBuffer = nullptr;
    std::future<void> renderThread;

    void RenderImage()
    {
        if (renderInProgress)
        {
            Render *render = GetRender();
            render->Terminate();
            renderThread.wait(); // Wait until can safely continue
        }

        renderAvail = false;
        renderInProgress = true;
        renderThread = std::async(std::launch::async, [&] {
            Render *render = GetRender();
            render->Rasterize();
            renderInProgress = false;
            renderAvail = true;
        });

        CleanDirty();
    }

public:
    RendererViewWindow(EditorProgram *mainProgram, const char *name, const bool defaultOpen)
        : EditorWindow(mainProgram, name, defaultOpen)
    {
        flags = ImGuiWindowFlags_NoScrollbar;
    }

    ~RendererViewWindow()
    {
        if (renderImage != nullptr) delete renderImage;
        if (renderBuffer != nullptr) delete[] renderBuffer;
    }

    void OnWindowGUI() override
    {
        Render *render = GetRender();

        if (ImGui::Button("Render"))
        {
            RenderImage();
        }

        ImGui::SameLine();
        ImGui::Checkbox("Live", &live);
        ImGui::SameLine();
        ImGui::ShowHelpMarker("Toggle Live Update, this feature will render whenever there is a change in current scene");

        ImGui::SameLine();
        ImGui::PushItemWidth(100);
        ImGui::InputDouble("Update Interval (ms)", liveUpdateInterval);
        ImGui::PopItemWidth();
        ImGui::SameLine();
        ImGui::ShowHelpMarker("If live update is on, this value will determine when the render will take place");

        if (renderBuffer != nullptr)
        {
            ImGui::SameLine();
            if (ImGui::Button("Save"))
            {
                auto filepath = SaveFileDialog("*.png");
                if (filepath.size() > 0)
                {
                    Texture2D::SaveRGBA32TextureAsPNG(renderBuffer, render->width, render->height, filepath);
                }
            }
        }

        while (live)
        {
            if (!IsDirty()) break;

            auto currentTime = mainProgram->GetTime();
            if ((currentTime - renderLastUpdate) < 0.500) break;

            RenderImage();
            break;
        }

        ImGui::Separator();
        ImGui::ProgressBar((float)render->GetProgress(), ImVec2(-1.0f,0.0f));

        if (renderImage != nullptr)
        {
            ImGui::ImageFitAvail(renderImage);
        }
        else
        {
            ImGui::Text("No Render Image");
            ImGui::SameLine();
            ImGui::ShowHelpMarker("You must render once");
        }

        // Update renderImage if a new render is avaliable and the render
        if (renderAvail)
        {
            renderAvail = false; // Consume newly renderImage
            if (renderImage == nullptr) renderImage = new GUITexture(); // Create
            if (renderBuffer != nullptr) delete[] renderBuffer; // Update renderBuffer

            // Render completed, now show the render
            renderBuffer = new RGBA32[render->width * render->height];
            render->ConvertToDisplay(renderBuffer);
            renderImage->Copy(renderBuffer, render->width, render->height);

            // Update last render update time stamp
            renderLastUpdate = mainProgram->GetTime();
        }
    }
};

class OutlinerWindow : public EditorWindow
{
    AnyPointer entryClicked;

    template<typename T>
    bool TreeEntry(const char* name, T* object = nullptr, bool leaf = true)
    {
        // Disable the default open on single-click behavior and pass in Selected flag according to our selection state.
        ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick |
                (Selection::IsSelected(object) ? ImGuiTreeNodeFlags_Selected : 0);

        // Leaf: The only reason we have a TreeNode at all is to allow selection of the leaf. Otherwise we can use BulletText() or TreeAdvanceToLabelPos()+Text().
        if (leaf)
            flags |= ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen; // ImGuiTreeNodeFlags_Bullet

        auto open = ImGui::TreeNodeEx((void*)(intptr_t)object, flags, name);
        if (ImGui::IsItemClicked())
            entryClicked = AnyPointer::Create<T>(object);

        return open;
    }

public:
    using EditorWindow::EditorWindow;

    void OnWindowGUI() override
    {
        Render *render = GetRender();
        Scene *scene = render->scene;

        if (ImGui::Button("Create"))
        {
            ImGui::OpenPopup("Create Primitive");
        }

        if (ImGui::BeginPopup("Create Primitive"))
        {
            //ImGui::Text("Primitives");
            //ImGui::Separator();
            if (ImGui::Selectable("Sphere"))
            {
                auto sphere = new Sphere();
                sphere->radius = 75;
                sphere->transform.location = { 0, (sphere->radius - 100), 400 };
                scene->objects.Append(sphere);
                MarkDirty();
            }
            if (ImGui::Selectable("Plane"))
            {
                auto plane = new Plane();
                plane->transform.location = { 0, -100, 0 };
                scene->objects.Append(plane);
                MarkDirty();
            }
            if (ImGui::Selectable("Cube"))
            {
                auto cube = Mesh::Cube();
                cube->transform.location = { 0, 0, 500 };
                cube->transform.scale.Set(100);
                scene->objects.Append(cube);
                MarkDirty();
            }
            ImGui::Separator();
            if (ImGui::Selectable("Point Light"))
            {
                auto light = new Light();
                scene->lights.Append(light);
                MarkDirty();
            }
            ImGui::EndPopup();
        }

        ImGui::SameLine();
        if (ImGui::Button("Import Obj"))
        {
            ImportModelObj(scene);
        }

        ImGui::SameLine();
        if (ImGui::Button("Import Texture"))
        {
            ImportTexture(scene);
        }

        ImGui::SameLine();
        if (ImGui::Button("Del"))
        {
            DeleteSelectedObjects(scene);
        }

//        ImGui::SameLine();
//        ImGui::ShowHelpMarker("Use this tree to select nodes that you want to edit");

        ImGui::Separator();

        entryClicked = AnyPointer();

        TreeEntry("Render Settings", render);
        TreeEntry("Camera", &scene->camera);

        // TODO better represent disabled objects
        if (TreeEntry("World", scene, false))
        {
            //ImGui::PushStyleVar(ImGuiStyleVar_IndentSpacing, ImGui::GetFontSize()*3);
            for(auto &obj : scene->objects)
            {
                TreeEntry(obj->name.c_str(), obj);
            }
            for(auto &light : scene->lights)
            {
                TreeEntry<Placeable>(light->name.c_str(), light);
            }
            //ImGui::PopStyleVar();
            ImGui::TreePop();
        }

        if (TreeEntry<void*>("Textures", nullptr, false))
        {
            for(auto &tex : scene->textures)
            {
                TreeEntry(tex->name.c_str(), tex);
            }
            ImGui::TreePop();
        }

        if (!entryClicked.IsNull())
        {
            // Update selection state. Process outside of tree loop to avoid visual inconsistencies during the clicking-frame.
            if (!ImGui::GetIO().KeyCtrl)
                Selection::Clear();

            Selection::AppendRaw(entryClicked);
        }
    }
};

class AboutWindow : public EditorWindow
{
public:
    Render *activeRenderer;

    AboutWindow(EditorProgram *mainProgram, const char *name, const bool defaultOpen) :
        EditorWindow(mainProgram, name, defaultOpen)
    {
        flags = ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_AlwaysAutoResize;
    }

    void OnWindowGUI() override
    {
        ImGui::Text("Simple Raytracer");
        ImGui::Text("Created by Felipe Jorge (fjp@ecomp.poli.br)");
    }
};

class SceneWindow : public EditorWindow
{
private:
    struct VertexData
    {
        float x, y, z;    // Vertex
        float nx, ny, nz; // Normal
        //float s0, t0;     // Texcoord0
        //float cr, cg, cb; // Color
    };


    double updateTimeStamp = 0;
    Camera editorCamera;

    GLuint renderedTexture = 0;
    GLuint framebuffer = 0;
    GLuint depthbuffer = 0;
    GLuint sharedVertexBuffer = 0;
    GLuint sharedIndiciesBuffer = 0;
    Array<VertexData> sharedVertexData;

public:
    SceneWindow(EditorProgram *mainProgram, const char *name, const bool defaultOpen) :
        EditorWindow(mainProgram, name, defaultOpen)
    {
        // Copy attributes from scene.camera to editorCamera
        Camera &camera = mainProgram->GetRender()->scene->camera;
        editorCamera.transform.location = camera.transform.location;
        editorCamera.transform.rotation = camera.transform.rotation;
        editorCamera.fieldOfView = camera.fieldOfView;
        editorCamera.focalDistance = camera.focalDistance;

        // Create a shader
    }

    ~SceneWindow()
    {
        if (framebuffer != 0) glDeleteFramebuffers(1, &framebuffer);
        if (depthbuffer != 0) glDeleteRenderbuffers(1, &depthbuffer);
        if (renderedTexture != 0) glDeleteTextures(1, &renderedTexture);
        if (sharedVertexBuffer != 0) glDeleteBuffers(1, &sharedVertexBuffer);
        if (sharedIndiciesBuffer != 0) glDeleteBuffers(1, &sharedIndiciesBuffer);
    }

    void Update(double deltaTime)
    {

    }

    void Render()
    {
       auto size = ImGui::GetContentRegionAvail();
       auto w = size.x;
       auto h = size.y;

       // Save current graphics state
       GLint savedTexture;
       GLint savedFramebuffer;
       glGetIntegerv(GL_TEXTURE_BINDING_2D, &savedTexture);
       glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &savedFramebuffer);

       // The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.

       if (framebuffer == 0) glGenFramebuffers(1, &framebuffer);
       glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

       // The depthbuffer
       if (depthbuffer == 0) glGenRenderbuffers(1, &depthbuffer);
       glBindRenderbuffer(GL_RENDERBUFFER, depthbuffer);
       glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, w, h);
       glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthbuffer);

       // Target texture
       if (renderedTexture == 0) glGenTextures(1, &renderedTexture);
       glBindTexture(GL_TEXTURE_2D, renderedTexture);
       glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);

       // Set "renderedTexture" as our colour attachement #0
       glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderedTexture, 0);

       // Set the list of draw buffers.
       GLenum drawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
       glDrawBuffers(1, drawBuffers); // "1" is the size of DrawBuffers

       // Always check that our framebuffer is ok
       if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
       {
           exit(-2);
       }

       // Scene render
       //glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
       glViewport(0, 0, w, h);
       //glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
       glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

       // TODO Push projection matrix
       // TODO Push view matrix

       // FIXME Look at the new Mesh definition
//       Scene *scene = GetRender()->scene;
//       for(Placeable *object : scene->objects)
//       {
//           if (!object->enabled) continue;

//           // TODO Push transform matrix
//           auto modelMatrix = object->transform.LocalToWorldMatrix();

//           auto mesh = dynamic_cast<Mesh*>(object);
//           if (mesh != nullptr)
//           {
//                sharedVertexData.ClearNoRealloc();

//                // TODO this sharedVertexData is quite slow, because this loop must be done every frame
//                auto vertexCount = mesh->verticies.Count();
//                for (int i = 0; i < vertexCount; ++i)
//                {
//                    VertexData vtxData;
//                    mesh->verticies[i].Copy((float*) &(vtxData.x));
//                    mesh->normals[i].Copy((float*) &(vtxData.nx));
//                    sharedVertexData.Append(vtxData);
//                }

//                // Bind and copy vertex data
//                if (sharedVertexBuffer == 0) glGenBuffers(1, &sharedVertexBuffer);
//                glBindBuffer(GL_ARRAY_BUFFER, sharedVertexBuffer);
//                glBufferData(GL_ARRAY_BUFFER, sizeof(VertexData) * sharedVertexData.Count(), sharedVertexData.Begin(), GL_STATIC_DRAW);

//#define BUFFER_OFFSET(i) ((char *)NULL + (i))
//                glEnableVertexAttribArray(0); // Verticies
//                glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), BUFFER_OFFSET(0));
//                glEnableVertexAttribArray(1); // Normals
//                glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), BUFFER_OFFSET(12));
////                glEnableVertexAttribArray(2); // UVs
////                glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(VertexData), BUFFER_OFFSET(24));

//                glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_SHORT, BUFFER_OFFSET(0));
//#undef BUFFER_OFFSET

//                // Same for triangles
//                glGenBuffers(1, &sharedIndiciesBuffer);
//                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sharedIndiciesBuffer);
//                glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned) * mesh->triangles.Count() * 3, mesh->triangles.Begin(), GL_STATIC_DRAW);
//           }

//           // TODO Render other kind of objects like planes an spheres
//       }

       // Restore state
       glBindFramebuffer(GL_FRAMEBUFFER, savedFramebuffer);
       glBindTexture(GL_TEXTURE_2D, savedTexture);
    }

    void OnWindowGUI() override
    {
        // Update state
        auto currentTime = mainProgram->GetTime();
        auto deltaTime = (currentTime - updateTimeStamp);
        Update(deltaTime);
        updateTimeStamp = currentTime;

        // Interface (Toolbar)

        // Render meshes in opengl then
        Render();

        // Display rendered image here
        ImGui::Image((void*)(intptr_t)renderedTexture, ImGui::GetContentRegionAvail());
    }
};


// Editor Program

EditorProgram::EditorProgram()
{
    activeRenderer = new Render(800, 600);
    activeRenderer->scene = BuildSampleScene();

    mainEditor = new MainEditor(this, "Desktop");

    const auto marginX = 5;
    const auto marginY = 5;
    const auto marginToolbar = 20;

    auto outliner = new OutlinerWindow(this, "Outliner", true);
    outliner->defalutX = marginX;
    outliner->defalutY = marginY + marginToolbar;
    outliner->defalutWidth = 300;
    outliner->defalutHeigth = 200;

    auto renderView = new RendererViewWindow(this, "Render", true);
    renderView->defalutX = outliner->defalutWidth + marginX * 2;
    renderView->defalutY = marginY + marginToolbar;
    renderView->defalutWidth = 1280 - renderView->defalutX - marginX;//500;
    renderView->defalutHeigth = 720 - renderView->defalutY - marginY;//400;

    auto inspector = new InspectorWindow(this, "Inspector", true);
    inspector->defalutX = outliner->defalutX;
    inspector->defalutY = outliner->defalutY + outliner->defalutHeigth + marginY;
    inspector->defalutWidth = outliner->defalutWidth;
    inspector->defalutHeigth = 720 - inspector->defalutY - marginY;

//    auto sceneView = new SceneWindow(this, "Scene", true);
//    sceneView->defalutX = renderView->defalutX + renderView->defalutWidth + marginX * 2;
//    sceneView->defalutY = marginY + marginToolbar;
//    sceneView->defalutWidth = 500;
//    sceneView->defalutHeigth = 400;

//    auto texturesView = new AssetsWindow(this, "Asset Browser", true);
//    texturesView->defalutX = renderView->defalutX;
//    texturesView->defalutY = renderView->defalutY + renderView->defalutHeigth + marginY;
//    texturesView->defalutWidth = 1280 - inspector->defalutWidth - marginY * 3;
//    texturesView->defalutHeigth = 720 - texturesView->defalutY - marginY;

    auto about = new AboutWindow(this, "About", false);

    editors.Append(outliner);
    editors.Append(renderView);
    editors.Append(inspector);
//    editors.Append(sceneView);
//    editors.Append(texturesView);
    editors.Append(about);
}

EditorProgram::~EditorProgram()
{
    delete mainEditor;
}

void EditorProgram::Init()
{
    // ImGui config
    ImGui::GetIO().IniFilename = NULL; // disable ini file (just for now)

    // Setup window
    glfwSetErrorCallback(EditorProgram::ErrorCallback);
    if (!glfwInit())
        exit(1);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#if __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
    window = (void*)glfwCreateWindow(1280, 720, "Application", NULL, NULL);
    glfwMakeContextCurrent((GLFWwindow*)window);
    glfwSwapInterval(1); // Enable vsync
    gl3wInit();

    // Setup ImGui binding
    ImGui_ImplGlfwGL3_Init((GLFWwindow*)window, true);
}

void EditorProgram::MainLoop()
{
    EmptyMainLoop([this]() {
//        Toolbar(&running);

//        if (ImGui::Begin("Render View", &running, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_AlwaysAutoResize))
//        {
//            ImGui::Image((void *)(intptr_t)(renderImage->id), ImVec2(renderImage->width, renderImage->height));
//        }
//        ImGui::End();

        mainEditor->OnGUI();
        for (auto &editor : editors) {
            editor->OnGUI();
        }
    });
}

void EditorProgram::EmptyMainLoop(EditorProgram::RenderFunction callback)
{
    while (!glfwWindowShouldClose((GLFWwindow*)window))
    {
        glfwPollEvents();
        ImGui_ImplGlfwGL3_NewFrame();

        callback();

        // Rendering
        int display_w, display_h;
        glfwGetFramebufferSize((GLFWwindow*)window, &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);
        glClearColor(0.45f, 0.55f, 0.60f, 1.00f);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui::Render();
        glfwSwapBuffers((GLFWwindow*)window);
    }
}

void EditorProgram::Cleanup()
{
    ImGui_ImplGlfwGL3_Shutdown();
    glfwTerminate();
}

Editor *EditorProgram::GetEditorByName(const char *name)
{
    for (auto &editor : editors) {
        if (strcmp(editor->name, name) == 0)
            return editor;
    }

    return NULL;
}

Render *EditorProgram::GetRender()
{
    return activeRenderer;
}

void *EditorProgram::GetWindow()
{
    return window;
}

void EditorProgram::SetWindowSize(int width, int height)
{
    glfwSetWindowSize((GLFWwindow*)window, width, height);
}

void EditorProgram::Close()
{
    glfwSetWindowShouldClose((GLFWwindow*)window, GLFW_TRUE);
}

double EditorProgram::GetTime()
{
    return glfwGetTime();
}

static Scene* EditorProgram::BuildSampleScene()
{
    // geometry
    Plane *floor = new Plane();
    //floor.normal = { 0, 1, 0 };
    //floor.origin = { 0, -100, 0 };
    floor->transform.rotation = { 0, 0, 0 };
    floor->transform.location = { 0, -100, 0 };
    //floor.material.metalic = 0.5;

//    Sphere *sphere = new Sphere();
//    sphere->radius = 75;
//    //sphere->origin = { 0, sphere->radius - 100, 400 };
//    //sphere->origin = { 0, 0, 0 };
//    sphere->transform.rotation = { 0, 45, 0 };
//    sphere->transform.scale = { 1.5, .5, 1 };
//    sphere->transform.location = { 0, (sphere->radius - 100), 400 };

    Mesh *mesh = new Mesh();
    //const auto meshFile = "../Meshes/Mophs_TheWard.obj";
    const auto meshFile = "../Meshes/Bunny.obj";
    if (Mesh::LoadFromObj(meshFile, *mesh))
    {
        // FIXME rotation { 0, 0, 0} and { 0, 180, 0} have the same result
        mesh->transform.rotation = { 0, 180, 0 };
        mesh->transform.scale = {15, 15, 15}; //{ 30, 30, 30 };
        mesh->transform.location = { 0, -104, 400 };
    }
    else
    {
        std::cerr << "Mesh::LoadFromObj() Failed: File not found \"" << meshFile << "\"" << std::endl;
        delete mesh; // free memory

        // Place a cube instead
        mesh = Mesh::Cube();
        mesh->transform.rotation = { 45, 45, 0 };
        mesh->transform.scale = { 50, 50, 50 };
        mesh->transform.location = { 0, 50, 400 };
    }    

    // lights
    Light *lamp = new Light();
    lamp->color = RGB::One();
    lamp->intensity = 1.0;
    lamp->transform.location = { 0, 300, 300 };

    //Light lamp1;
    //lamp1.intensity = 0.35;
    //lamp1.origin = { -100, 500, -100 };

    Scene *scene = new Scene();
    scene->objects.Append(floor);
    //scene->objects.Append(sphere);
    scene->objects.Append(mesh);

    scene->ambientColor = RGB::Zero();
    scene->lights = { lamp }; //, &lamp1 };

    Camera *camera = &(scene->camera);
    camera->transform.location = { 0, 200, 0};
    camera->transform.rotation = { 30, 0, 0};

    return scene;
}

static void EditorProgram::ErrorCallback(int error, const char* description)
{
    std::cerr << "Error " << error << ":" << description << std::endl;
}


