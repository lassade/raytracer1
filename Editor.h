#pragma once

#include <functional>
#include "Array.h"
#include "Raycaster.h"
#include "Alias.h"

class Editor;
class EditorWindow;
class EditorProgram;

class GUITexture
{
public:
    unsigned int id = 0;
    int width = -1, height = -1;

    GUITexture();
    ~GUITexture();

    void Copy(void* data);
    void Copy(void* data, int width, int height);
};

class EditorProgram
{
private:
    Array<Editor*> editors;
    Editor *mainEditor;
    Render *activeRenderer;
    void *window;

    static void ErrorCallback(int error, const char* description);
    static Scene* BuildSampleScene();

public:
    typedef std::function<void()> RenderFunction;

    EditorProgram();
    ~EditorProgram();

    void Init();
    void MainLoop();
    void EmptyMainLoop(RenderFunction callback);
    void Cleanup();
    Editor* GetEditorByName(const char* name);
    Render* GetRender();
    void* GetWindow();
    void SetWindowSize(int width, int height);
    void Close();

    static double GetTime();
};
