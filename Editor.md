# Editor

## Features

1. Save and load scenes in a open source format (glTF? or some more simpler)
2. Place primitives and load .objs and delete them
3. World Object(scene) will have it's own properties, to make various configurations
4. OpenGL scene preview
5. Render a select buffer at quarter resolution

## Windows

1. Scene View
2. Camera View
3. Scene Hieracy
4. Object Ispector (Properties Panel)
5. Toolbar
