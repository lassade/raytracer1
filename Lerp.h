#pragma once

#include "Util.h"

template<typename T>
inline T LerpLinear(T a, T b, double t)
{
    return (a * t) + (b * (1.0 - t));
}

template<typename T>
inline T LerpQuad(T a, T b, T c, double t)
{
    return LerpLinear(LerpLinear(a, b, t), LerpLinear(b, c, t), t);
}

//template<typename T>
//inline T Lerp(double t, T... args)
//{
//}

template<typename T>
inline T LerpLinearSafe(T a, T b, double t)
{
    t = Max(Min(t, 1.0), 0.0);
    return LerpLinear(a, b, t);
}

template<typename T>
inline T LerpQuadSafe(T a, T b, T c, double t)
{
    t = Max(Min(t, 1.0), 0.0);
    return LerpQuad(a, b, c, t);
}
