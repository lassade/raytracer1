#include "Matrix.h"

Vec3 operator *(const Mat<4, 4>& m, const Vec3& v)
{
	Vec4 v4(v);
	v4 = m * v4;
	return static_cast<Vec3>(v4);
}
