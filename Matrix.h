#pragma once

#include <type_traits>
#include "Vector.h"

template<unsigned W, unsigned H>
struct Mat {
private:
	double m[W*H];
public:
	// column and line
	double& operator ()(const unsigned i, const unsigned j) { return m[W*j + i]; }
	const double operator ()(const unsigned i, const unsigned j) const { return m[W*j + i]; }

	static Mat<W, H> Eye() {
		Mat<W, H> r = Zero();
		const unsigned lenght = (W < H) ? W : H;
		for (unsigned i = 0; i < lenght; i++)
			r(i, i) = 1;
		return r;
	}
	static Mat<W, H> Zero()
	{
		Mat<W, H> r;
		for (unsigned i = 0; i < W*H; i++) r.m[i] = 0;
		return r;
	}

	template < typename = typename std::enable_if< (W == 4) && (H == 4) >::type >
	inline const Vec3 MultiplyPoint(Vec3 v) const
	{
		Vec4 v4(v);
        v4(3) = 1;
		v4 = (*this) * v4;
        v4 = v4 / v4(3);
		return static_cast<Vec3>(v4);
	}
	template < typename = typename std::enable_if< (W == 4) && (H == 4) >::type >
	const Vec3 MultiplyVector(Vec3 v) const
	{
		return (*this) * v;
    }
};

template<unsigned W, unsigned H, unsigned N>
Mat<N, H> operator *(const Mat<W, H>& a, const Mat<N, W>& b)
{
	Mat<N, H> r = Mat<N, H>::Zero();

	for (unsigned j = 0; j < H; j++)
		for (unsigned i = 0; i < N; i++)
			for (unsigned k = 0; k < W; k++)
				r(i, j) += a(k, j) * b(i, k);

	return r;
}

// TODO make it generic, multiply Mat<W, H> by a Vec<W>
template<unsigned W, unsigned H>
Vec<double, W> operator *(const Mat<W, H>& m, const Vec<double, W>& v) {
	Vec<double, W> r = Vec<double, W>::Zero();

	for (unsigned j = 0; j < H; j++)
		for (unsigned i = 0; i < W; i++)
            r(j) += m(i, j) * v(i);

	return r;
}

Vec3 operator *(const Mat<4, 4>& m, const Vec3& v);

typedef Mat<4, 4> Mat44;
