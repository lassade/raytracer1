#define NO_ERITOR 0

#include <iostream>
#include <cmath>
#include <vector>
#include <cassert>

#include "Lerp.h"
#include "Raycaster.h"
#include "Editor.h"

#if NO_ERITOR
#include <imgui.h>

namespace ImGui {
void Image(GUITexture* tex);
}
#endif

int main(int argc, char* args[])
{
    EditorProgram program;
    program.Init();

    // Main loop
#if NO_ERITOR
    Render *render = program.GetRender();
    Scene *scene = render->scene;
    GUITexture *renderImage = new GUITexture();
    ARGB32 *renderBuffer = new ARGB32[render->width * render->height];
    const unsigned char rgba[] = {3, 0, 1, 2}; // pixels order
    unsigned frame  = 0;
    unsigned frameCount = 64;
    double timeStamp = 0;
    double targetFrameRate = 1.0 / 60;

    program.SetWindowSize(render->width, render->height);
    program.EmptyMainLoop([&]()
    {
        // keep at 60 fps
        double currentTimeStamp = EditorProgram::GetTime();
        double lapsedTime = currentTimeStamp - timeStamp;
        if (lapsedTime < targetFrameRate)
        {
            // do not render is too soon
            return;
        }
        timeStamp = currentTimeStamp;

        // Render
        render->Rasterize();
        render->ConvertToDisplay(renderBuffer, rgba);
        renderImage->Copy(renderBuffer, render->width, render->height);

        // Animate
        {
            // frame data
            frame = (frame + 1) % frameCount;
            double normalizedTime = frame / (double)frameCount;

            Transform *transform = &(scene->objects[1]->transform);
            transform->rotation(1) = normalizedTime * 360 + 180;

            //transform->scale = {1, 1, 1};
            //transform->rotation(2) = normalizedTime * 360;

            //double s = Min(abs((normalizedTime - 0.5) * 2), 0.5);
            //transform->scale = {(1+s), (1-s), 1};
        }

        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0, 0));
        ImGui::SetNextWindowPos(ImVec2(0, 0));
        ImGui::SetNextWindowSize(ImGui::GetIO().DisplaySize);
        ImGui::Begin("Render (Bordless)", NULL, ImVec2(0, 0), 0.0f,
                     ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoResize|ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoScrollbar|ImGuiWindowFlags_NoScrollWithMouse);
        ImGui::Image(renderImage);
        ImGui::End();
        ImGui::PopStyleVar();
    });

    // Clean up
    delete[] renderBuffer;
    delete renderImage;
    program.Cleanup();

#else
    program.MainLoop();
    program.Cleanup();
#endif

	return 0;
}

