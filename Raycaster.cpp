#include <cmath>
#include <limits>
#include <thread>
#include <mutex>
#include <random>

#include "Alias.h"
#include "Raycaster.h"
#include "Util.h"
#include "Lerp.h"

Ray::Ray()
{
}

Ray::Ray(const Ray & other)
	: origin(other.origin), direction(other.direction)
{
}

Ray::Ray(Vec3 origin, Vec3 direction)
	: origin(origin), direction(direction)
{
}

Vec3 Ray::Eval(double t) const {
	return origin + (direction * t);
}

bool Placeable::Raycast(const Ray &ray, Impact &impact, const double distance, const double bias)
{
    return false;
}

bool Placeable::CheckBoundBox(const Ray &ray)
{
    // TODO
    return true;
}

void Placeable::Prepare()
{
	//Transform rs(transform);
	//rs.location = { 0, 0, 0 };
	//local = rs.WorldToLocalMatrix();
	//world = rs.LocalToWorldMatrix();

	local = transform.WorldToLocalMatrix();
	world = transform.LocalToWorldMatrix();
}

Ray Placeable::InverseTransformRay(const Ray & ray)
{
	Vec3 direction = local.MultiplyVector(ray.direction);
    // WARINING do not normalize the directio, since it will change the Impact.distance
    //direction.Normalize();
	return Ray(local.MultiplyPoint(ray.origin), direction);
}

void Placeable::TransformImpact(Impact & impact)
{
	impact.point = world.MultiplyPoint(impact.point);
    impact.normal = world.MultiplyVector(impact.normal);
    impact.normal.Normalize();
}

Plane::Plane()
{
    name = "Plane";
}

bool Plane::Raycast(const Ray & ray, Impact & impact, const double distance, const double bias)
{
	Ray r = InverseTransformRay(ray);
    double t = -Vec3::Dot((r.origin - origin), normal) / Vec3::Dot(r.direction, normal);
    if (t >= bias && t <= distance)
	{
        Vec3 p = r.Eval(t);
        impact.point = p;
        impact.normal = normal;
        impact.triangle = -1;
        impact.distance = t;
        impact.object = this;

        // use the transform to ajust the texture warp
        Vec2 uv;
        uv(0) = p(0) * 0.01; // let the overflow do his magic
        uv(1) = p(2) * 0.01;
        impact.uv = uv;

		TransformImpact(impact);
		return true;
	}

	return false;
}

Sphere::Sphere()
{
    name = "Sphere";
}

bool Sphere::Raycast(const Ray & ray, Impact & impact, const double distance, const double bias)
{
	Ray r = InverseTransformRay(ray);
	Vec3 ec = r.origin - origin;
    double dec = Vec3::Dot(r.direction, ec);
    double dd = Vec3::Dot(r.direction, r.direction);

    double delta = (dec * dec) - (dd * (Vec3::Dot(ec, ec) - (radius * radius)));
	if (delta >= 0)
	{
		double sqrtd = sqrt(delta);
		double a = (-dec + sqrtd) / dd;
		double b = (-dec - sqrtd) / dd;
		double t = (a > b) ? b : a; // choose the min

        if (t < bias || t > distance) return false;

		Vec3 p = r.Eval(t);
        impact.point = p;
        impact.normal = (p - origin) / radius;
        impact.triangle = -1;
        impact.distance = t;
        impact.object = this;

        Vec2 uv;
        //p.Normalize();
        p = p / radius;
        uv(0) = atan2(p(0), p(2)) / (2*PI) + 0.5;
        uv(1) = p(1) * 0.5 + 0.5;
        impact.uv = uv;

		TransformImpact(impact);
		return true;
	}

	return false;
}

bool Scene::Raycast(const Ray &ray, Impact &impact, const double bias)
{
	// find which object the camera seeing at the current pixel(i, j)
	bool hit = false;
    double distance = std::numeric_limits<double>::max();
	for (auto &object : objects)
    {
        if (!object->enabled) continue;

        if (object->Raycast(ray, impact, distance, bias))
        {
            hit = true;
            distance = Min(impact.distance, distance);
        }
	}

	return hit;
}

Vec3 Scene::Reflect(const Vec3 & direction, const Vec3 & normal)
{
    return direction - normal * (2 * Vec3::Dot(direction, normal));
}

bool Scene::Refract(const Vec3 & d, const Vec3 & normal, const double n, const double nt, Vec3 & t)
{
    double cost = Vec3::Dot(d, normal); // cossine of theta
	double delta = 1 - (((n*n) * (1 - (cost * cost))) / (nt*nt));

	if (delta < 0) return false;

	t = (((d - (normal * cost)) * n) / nt) - (normal * sqrt(delta));
	return true;
}

RGB Scene::TracePhoton(const Ray &ray, unsigned depth = 64, unsigned renderType = 0)
{
    if (depth <= 0)
        return RGB::Zero();

	Impact impact;
    impact.object = nullptr;
    if (!Raycast(ray, impact, 0.001))
    {
        return RGB::Zero();
    }

    RGB color = RGB::Zero();

    if (renderType == Render::RENDER_UVS)
    {
        for (int i = 0; i < 2; ++i)
            color(i) = impact.uv(i);
        return color;
    }

    if (renderType == Render::RENDER_NORMALS)
    {
        for (int i = 0; i < 3; ++i)
            color(i) = (impact.normal(i) * 0.5) + 0.5;
        return color;
    }

    auto object = (Placeable*)impact.object;
    auto material = &(object->material);

    color = ambientColor;
    for (Light* light : lights)
    //for (auto light = lights->Begin(); light != lights->End(); light++)
	{
        if (!light->enabled) continue;

        // for each sample defined by the light
        for (auto sampleIndex = light->Begin(); sampleIndex != light->End(); sampleIndex++)
        {
            Vec3 lightDirection;
            double lightIntensity = 0;
            double lightDistance = 0;
            if (!light->LightDirection(sampleIndex, impact.point, lightDirection, lightIntensity, lightDistance)) continue;

            // Calcule shadows for direct light
            Impact block;
            bool shadow = false;
            if (light->castShadows && Raycast(Ray(impact.point, lightDirection), block, 0.001))
            {
                // If there's something in between the object and the light source cast a shadow
                if (block.distance < lightDistance) shadow = true;
            }

            // Angle on incidence
            double lightAngle = Max(0.0, Vec3::Dot(lightDirection, impact.normal));

            // Lambert pass
            if(!shadow && (material->type != Material::TYPE_GLASS))
            {
                auto objectColor = material->diffuseColor;
                if (material->diffuseMap != nullptr)
                {
                    objectColor *= material->diffuseMap->SampleBilinear(impact.uv(0), impact.uv(1));
                }
                color += (lightIntensity * lightAngle) * (light->color * objectColor);
            }

            // Blinn-Phong pass
            if(material->type == Material::TYPE_PHONG)
            {
                if (!shadow)
                {
                    auto h = lightDirection - ray.direction;
                    h.Normalize();
                    color += material->specularColor * light->color * (lightIntensity * pow(Max(0.0, Vec3::Dot(impact.normal, h)), material->specularIntensity));
                }
            }
        }
    }

    // Reflections
    if(material->type == Material::TYPE_PHONG)
    {
        auto reflection = Reflect(ray.direction, impact.normal);
        // Mirror reflections
        auto refectColor = TracePhoton(Ray(impact.point, reflection), depth - 1, renderType);
        //color = (1 - ref) * color + (ref * refectColor);
        color += material->reflectionIntensity * (material->specularColor * refectColor);
    }

    return color;
}

void Scene::Prepare()
{
    camera.Prepare();

    for (Placeable *obj : objects)
        obj->Prepare();

    for (Light *light : lights)
        light->Prepare();
}

Mesh::Mesh()
{
    name = "Mesh";
}

Mesh::~Mesh()
{
    if (node != nullptr) delete node;
}

bool Mesh::Raycast(const Ray &ray, Impact &impact, const double distance, const double bias)
{
    Ray r = InverseTransformRay(ray);
    OptimizedRay optray(r);
    double d = distance; // allow change

    if (node->Raycast(optray, impact, d, bias))
    {
        TransformImpact(impact);
        impact.object = this;
        return true;
    }

    return false;
}

void Mesh::RebuildKDTree()
{
    if (node != nullptr) delete node;
    node = KDNode::Build(*this);
}

// FIXME there is some thing worng with this
bool Mesh::LoadFromObj(const char * path, Mesh& mesh)
{
	FILE *file = fopen(path, "r");
	if (file == NULL) return false;

	// parser main loop
	{
        Array<Vec3> &verticies = mesh.verticies;
        Array<Vec3> &normals = mesh.normals;
        Array<Vec2> &uvs = mesh.uvs;
        Mesh::TriangleIndex triangle;

		// count how many of each list the mesh gonna need
		int state = 0;
		char token[64];
		unsigned i = 0;
		Vec3 vertex;
		Vec2 point; // used for uvs
        int tokenInt;
        int character = 0;
        int characterLast = 0;
        int characterLastSplit = 0;
		while (1)
		{
			if (feof(file))
				break; // end of file reached

            characterLast = character;
            character = fgetc(file);
            if (character == ' ' || character == '\n' || character == '/')
			{
				token[i] = '\0';
				// parse token
				switch (state)
				{
				case 0:
					if (token[0] == 'v')
					{
						if (i == 1) state = 10; // vertex
						else if (token[1] == 'n') state = 20; // normal
						else if (token[1] == 't') state = 40; // uv
					}
                    else if (token[0] == 'f')
                    {
                        // Clear any indices
                        triangle.v = {-1, -1, -1};
                        triangle.u = {-1, -1, -1};
                        triangle.n = {-1, -1, -1};
                        state = 60; // triangle
                    }
					else state = 80; // ignore line
					break;

#define AddVecComponent(VecElem, Index, NextState) VecElem(Index) = atof(token); state = NextState
#define FinalAddVecComponent(VecArray, VecElem, Index, NextState) AddVecComponent(VecElem, Index, NextState); VecArray.Append(VecElem)

                // Verticies
                case 10: AddVecComponent(vertex, 0, 11); break;
                case 11: AddVecComponent(vertex, 1, 12); break;
                case 12: FinalAddVecComponent(verticies, vertex, 2, (character == '\n') ? 0 : 80); break;

                // Normals
                case 20: AddVecComponent(vertex, 0, 21); break;
                case 21: AddVecComponent(vertex, 1, 22); break;
                case 22: FinalAddVecComponent(normals, vertex, 2, (character == '\n') ? 0 : 80); break;

                // UVs
                case 40: AddVecComponent(point, 0, 41); break;
                case 41: FinalAddVecComponent(uvs, point, 1, (character == '\n') ? 0 : 80); break;

#undef AddVecComponent
#undef FinalAddVecComponent

#define TokenToInt() tokenInt = atoi(token) - 1
#define ValidateToken() if (strlen(token) > 0)
#define AddTriangleIndexElement(Element, Index, NextState) ValidateToken(){ TokenToInt(); triangle.Element(Index) = tokenInt; } state = NextState
#define SkipableAddTriangleIndexElement(Element, Index, NextState, SkipState) if (characterLastSplit != ' ') { AddTriangleIndexElement(Element, Index, NextState); } else { state = SkipState; }

                case 60: AddTriangleIndexElement(v, 0, 61); break; // vertex triangle
                case 61: SkipableAddTriangleIndexElement(u, 0, 62, 63); break; // uv
                case 62: SkipableAddTriangleIndexElement(n, 0, 63, 63); break; // normal
                case 63: AddTriangleIndexElement(v, 1, 64); break;
                case 64: SkipableAddTriangleIndexElement(u, 1, 65, 66); break;
                case 65: SkipableAddTriangleIndexElement(n, 1, 66, 66); break;
                case 66: AddTriangleIndexElement(v, 2, 67); break;
                case 67: SkipableAddTriangleIndexElement(u, 2, 68, -1);
                    if (state == -1)
                    {
                        state = (character == '\n') ? 0 : 80;
                        mesh.triangles.Append(triangle);
                    }
                    break;
                case 68:
                    state = (character == '\n') ? 0 : 80;
                    SkipableAddTriangleIndexElement(n, 2, state, state);
                    mesh.triangles.Append(triangle);
                    break;

//                case 60: ValidateToken(){ TokenToInt(); triangle.v(0) = tokenInt; } state = 61; break; // vertex triangle
//                case 61: ValidateToken(){ TokenToInt(); triangle.u(0) = tokenInt; } state = 62; break; // uv
//                case 62: ValidateToken(){ TokenToInt(); triangle.n(0) = tokenInt; } state = 63; break; // normal
//                case 63: ValidateToken(){ TokenToInt(); triangle.v(1) = tokenInt; } state = 64; break;
//                case 64: ValidateToken(){ TokenToInt(); triangle.u(1) = tokenInt; } state = 65; break;
//                case 65: ValidateToken(){ TokenToInt(); triangle.n(1) = tokenInt; } state = 66; break;
//                case 66: ValidateToken(){ TokenToInt(); triangle.v(2) = tokenInt; } state = 67; break;
//                case 67: ValidateToken(){ TokenToInt(); triangle.u(2) = tokenInt; } state = 68; break;
//                case 68: ValidateToken(){ TokenToInt(); triangle.n(2) = tokenInt; }
//                    mesh.triangles.Append(triangle);
//                    state = (c == '\n') ? 0 : 80;
//					break;

#undef SkipableAddTriangleIndexElement
#undef AddTriangleIndexElement
#undef TokenToInt
#undef ValidateToken

                case 80: // ignore until last line
                    if (character == '\n') state = 0;
					break;

				default:
					break;
				}
				i = 0;
                characterLastSplit = character;
			}
			else
			{
                token[i] = character; // build token
				i++;
            }
		}
		fclose(file);
	}

    Path filePath(path);
    mesh.name = filePath.stem().string();
    mesh.RebuildKDTree();
	return true;
}

Mesh* Mesh::Cube()
{
	Mesh *mesh = new Mesh();

    mesh->colors =
    {
		{ 0.0, 1.0, 0.0 },
		{ 0.0, 0.0, 0.0 },
		{ 1.0, 0.0, 0.0 },
		{ 1.0, 1.0, 0.0 },
		{ 0.0, 1.0, 1.0 },
		{ 0.0, 0.0, 1.0 },
		{ 1.0, 0.0, 1.0 },
		{ 1.0, 1.0, 1.0 }
	};

    mesh->verticies =
    {
		{ 1, 1, -1 },
		{ 1, -1, -1 },
		{ -1, -1, -1 },
		{ -1, 1, -1 },
		{ 1, 1, 1 },
		{ 1, -1, 1 },
		{ -1, -1, 1 },
		{ -1, 1, 1 }
	};

    mesh->uvs = {
        { 0, 0 },
        { 1, 0 },
        { 1, 1 },
        { 0, 1 }
    };

    // FIXME UVs aren't right
#define MakeTriangleIndex(a, b, c) { { a, b, c, }, { -1, -1, -1, }, { (a % 4), (b % 4), (c % 4), } }
#define MakeQuadIndex(a, b, c, d) MakeTriangleIndex(a, b, c), MakeTriangleIndex(a, c, d)
	mesh->triangles = {
        MakeQuadIndex(0, 1, 2, 3),
        MakeQuadIndex(4, 7, 6, 5), // MakeQuadIndex(4, 7, 6, 5),
        MakeQuadIndex(0, 4, 5, 1), // MakeQuadIndex(0, 4, 5, 1),
        MakeQuadIndex(1, 5, 6, 2), // MakeQuadIndex(1, 5, 6, 2),
        MakeQuadIndex(2, 6, 7, 3), // MakeQuadIndex(2, 6, 7, 3),
        MakeQuadIndex(4, 0, 3, 7) //MakeQuadIndex(4, 0, 3, 7)
	};
#undef MakeTriangleIndex
#undef MakeQuadIndex

    mesh->RebuildKDTree();
    mesh->name = "Cube";
	return mesh;
}



Render::Render(unsigned width, unsigned height)
    : width(width), height(height)
{
}

Render::~Render()
{
    delete[] renderedImage;
}

void Render::Rasterize()
{
    terminate = false; // Ignore if running in the same thread
    scene->Prepare();

    // Realocate rendered image if needed
    auto pc = width * height;
    if (pc != pixelsCount)
    {
        pixelsCount = pc;

        if (renderedImage != nullptr)
        {
            delete[] renderedImage;
        }

        renderedImage = new RGB[pixelsCount];
    }

    Camera* camera = &(scene->camera);
    Vec3 forward = camera->forward, up = camera->up, right = camera->right;
    Vec3 origin = camera->origin;

    double d = camera->focalDistance;
    double w = 0, h = 0;
    {
        Vec2 plane = camera->ProjectionPlane(width, height);
        w = plane(0);
        h = plane(1);
    }

    done = false;
    renderProgress = 0.0;
    double pixelProgressDelta = 1.0 / pc; // each pixel rendere adds this ammount to the progress
    //auto antialiasSamples = Max(antialias, 1);
    //auto antialiasContribuition = 1.0 / antialiasSamples;

    auto renderThreadsCount = Max(threadsCount, 1u);
    auto renderThreads = new std::thread[renderThreadsCount];
    std::mutex progressLock;
//    std::mutex log;

    //int t = 0;
    for (unsigned t = 0; t < renderThreadsCount; t++)
    {
        renderThreads[t] = std::thread([&] (unsigned threadIndex, unsigned threadsTotal) {
            // Thread Begin
            Ray ray;
            ray.origin = origin;
            const unsigned renderRegion = 64;
            unsigned nextRenderRegion = threadsTotal * renderRegion;
            for (unsigned wStart = (threadIndex * renderRegion); wStart < width; wStart += nextRenderRegion)
                for (unsigned hStart = 0; hStart < height; hStart += renderRegion)
                {
                    auto partialProgress = 0.0;
                    auto wEnd = Min(wStart + renderRegion, width);
                    auto hEnd = Min(hStart + renderRegion, height);

//                    log.lock();
//                    std::cout << "Thread " << threadIndex << " Rect (" << w << ", " << h << ", " << wEnd << ", " << hEnd << ")" << std::endl;
//                    log.unlock();

                    for (int i = wStart; i < wEnd; i++)
                        for (int j = hStart; j < hEnd; j++)
                        {
                            //for (int k = 0; k < antialiasSamples; k++)
                            {
                                if (terminate)
                                {
                                    done = true;
                                    terminate = false;
                                    return;
                                }

                                // calcule camera ray
                                double u = -w + (2 * w * (i + 0.5) / width);
                                double v = -h + (2 * h * (j + 0.5) / height);

                                ray.direction = (right * u) + (up * v) + (forward * d);
                                ray.direction.Normalize();

                                renderedImage[width * j + i] = scene->TracePhoton(ray, 8, renderType);// * antialiasContribuition;
                                partialProgress += pixelProgressDelta; // Maybe this will cause issues
                            }
                        }

                    progressLock.lock();
                    renderProgress += partialProgress;
                    progressLock.unlock();
                }
//
//            std::cout << "Thread " << threadIndex << " Ended" << std::endl;
//            log.unlock();
            // Thread End
        }, t, renderThreadsCount);
    }

    for (int t = 0; t < renderThreadsCount; t++)
        renderThreads[t].join();

    delete[] renderThreads;

    done = true;
    terminate = false;
}

//void Render::RasterizeMultiThread(Render::OnRenderCopleted complete = nullptr)
//{
//    std::thread single([this]() {
//        Rasterize();
//    });

//    single.join();
//}

void Render::ConvertToDisplay(RGBA32 *display)
{
    //const float gamma = 1.0 / 2.2;

    auto exposure = scene->camera.exposure;
    RGB hdr; RGBA32 ldr;
    unsigned size = width * height;
    for (unsigned i = 0; i < size; i++)
    {
        hdr = *(renderedImage + i);

        if (renderType == RENDER_DEFAULT)
        {
            for (unsigned j = 0; j < 3; ++j)
            {
                // exponential tone maping
                double mapped = 1.0 - exp(-hdr(j) * exposure);

#if 0 // Disable or enabled crazy tone mapping
                // FIXME how use exporsure here?
                // TODO more tone mapping algorithms check this link : https://mynameismjp.wordpress.com/2010/04/30/a-closer-look-at-tone-mapping/
                // reinhard tone mapping
                // double mapped = hdr(j) / (hdr(j) + 1.0);

                // gamma correction
                hdr(j) = pow(mapped, gamma); // renderedImage is in linear space
#else
                hdr(j) = mapped;
#endif
            }
        }

        for (unsigned j = 0; j < 3; ++j) ldr(j) = Clamp((int)(hdr(j) * 255), 0, 255);
        ldr(3) = 255;

        //display[size - i - 1] = ldr; // SDL buffer expects the image to be writen in reverse like a BMP
        display[size - i - 1] = ldr; // SDL buffer expects the image to be writen in reverse like a BMP
    }
}

double Render::GetProgress()
{
    return renderProgress;
}

bool Render::IsDone()
{
    return done;
}

void Render::Terminate()
{
    terminate = true;
}

void Camera::Prepare()
{
    Mat44 world = transform.LocalToWorldMatrix();
    forward = world.MultiplyVector({ 0, 0, 1 });
    up = world.MultiplyVector({ 0, 1, 0 });
    right = world.MultiplyVector({ 1, 0, 0 });

//    for (int i = 0; i < 3; ++i)
//        origin(i) = world(3, i);
    origin = Transform::Position(world);
}

Vec2 Camera::ProjectionPlane(unsigned width, unsigned height)
{
    double fovRad = DEG2RAD * fieldOfView;
    double w = focalDistance * tan(fovRad * 0.5); // fiexed guess is projection plane

    Vec2 plane;
    plane(0) = w;
    plane(1) = (w / width) * height;

    return plane;
}

Light::Light()
{
    name = "Point Light";
}

bool Light::LightDirection(Index sampleIndex, Vec3 &origin, Vec3 &direction, double &intensity, double &distance)
{
    static std::default_random_engine generator;
    static std::uniform_real_distribution<double> uniform(0.0,1.0);

    // FIXME Not implemented
    if (lightType == LT_SPOT)
        return false;

    intensity = this->intensity;

    if (lightType == LT_DIRECTIONAL)
    {
        direction = world.MultiplyVector({0, 1, 0});
        direction.Normalize(); // Have to normalize this guy
        distance = -1.0; // If any thing is between the light source and the object cast a shadow
    }
    else
    {
        Vec3 location = transform.location;

        switch (lightType) {
        case LT_POINT:
            if (radius > 0)
            {
                Vec3 omni;
                for (int i = 0; i < 3; ++i) omni(i) = uniform(generator);
                //omni *= (1.0 / 3.0); // Over all the geometry including inside points, really hard to take surface points
                omni.Normalize(); // Only over the surface
                location += omni * radius;
            }
            break;
        case LT_DIRECTIONAL:
            break;
        case LT_AREA:
            break;
        case LT_SPOT:
            break;
        default:
            break;
        }

        Vec3 towards = location - origin;
        distance = towards.Lenght();
        direction = towards / distance;

        intensity /= End();
        switch (decayType) {
        case DT_LINEAR:
            intensity *= 1.0 - Min(distance / reach, 1.0);
            break;

        case DT_EXPONENTIAL: // Ruled by the inverse square law
            //intensity *= Min(1.0 / ((distance * distance) * 0.0001), 1.0);
            intensity *= Clamp(1.0 - (distance*distance)/(reach*reach), 0.0, 1.0);
            break;

        default:
            break;
        }
    }

    return true;
}

void Light::Prepare()
{
    Placeable::Prepare();

//    // TODO create various Lights distributed by Monte carlo algorithm
//    switch (lightType) {
//    case LT_POINT:
//        break;

//    case LT_DIRECTIONAL:
//        // TODO Implement
//        break;

//    case LT_SPOT:
//        // TODO Implement
//        break;

//    case LT_AREA:
////        // Vertices of the area light
////        Vec3 a = world.MultiplyPoint({-0.5 * size(0), 0, -0.5 * size(1)}),
////             b = world.MultiplyPoint({ 0.5 * size(0), 0, -0.5 * size(1)}),
////             c = world.MultiplyPoint({ 0.5 * size(0), 0,  0.5 * size(1)}),
////             d = world.MultiplyPoint({-0.5 * size(0), 0,  0.5 * size(1)});

////        // Calculate the side leght
////        int count = Max((int)ceil(sqrt(samplesCount)), 0);
////        // For each sample, interpolate a point inside the square
////        for (int i = 0; i < count; ++i)
////        {
////            double u = (i + 0.5) / (double)count;
////            Vec3 ab = LerpLinear(a, b, u);
////            Vec3 cd = LerpLinear(c, d, u);
////            for (int j = 0; j < count; ++j)
////            {
////                double v = (j + 0.5) / (double)count;
////                samples.Append(LerpLinear(ab, cd, v));
////            }
////        }
//        break;

//    default:
//        break;
//    }
}

Light::Index Light::Begin()
{
    return 0;
}

Light::Index Light::End()
{
    switch (lightType) {
    case LT_POINT:
        if (radius <= 0) return 1; // Do not render multiple samples if don't need to
        break;
    case LT_DIRECTIONAL:
        break;
    case LT_AREA:
        break;
    case LT_SPOT:
        break;
    default:
        break;
    }
    return samplesCount;
}
