# Simple Raytracer 1

# FIXME

1. Need to fix the gamma/linear space model of the renderer

# TODO

1. More materials
1.1. Lambert
1.2. Phong (Reflection)
1.3. Surface
1.4. Emissive (No clue how to make it happen)
1.5. Translucent

2. Add exponential light decay

2. Extra Materials properties (Color / Texture)
2.1. Diffuse
2.2. Specular
2.3. Normals

3. Find a faster triangle raycast function

4. Use triangle stripes (better memory usage)

5. Add other Light types (Area, Spot, Directional)

6. Glass Like Difractions

7. Add more HDR tone mapping algorithms

# Ideas

1. Maybe each triangle can be transformed into a 2D triangle given a Plane and the ray to the same space
2. Vulkan Compute Shaders / Well use OpenGL's since my PC does not support
