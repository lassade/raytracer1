 
#include "Texture2D.h"


Texture2D::Texture2D()
{
}

Texture2D::~Texture2D()
{
    // Free all textures
    for (int i = 0; i < GetMipmapCount(); ++i)
    {
        delete[] mipmaps[i];
    }
}

RGBA Texture2D::SampleBilinear(double x, double y, const int mipmapLevel)
{
    auto size = GetTextureSize(mipmapLevel);
    int w = size(0), h = size(1);

    // Input coordinates are normalized
    x *= w;
    y *= h;

    // Texture warp
    switch (warpMode) {
    case WARP_CLAMP:
        x = Clamp<double>(x, 0, w);
        y = Clamp<double>(y, 0, h);
        break;
    default:
        auto rx = fmod(x, w);
        x = (rx >= 0)? rx : w + rx;
        auto ry = fmod(y, h);
        y = (ry >= 0)? ry : h + ry;
        break;
    }

    // a - b
    // |   |
    // c - d
    RGBA color;
    {
        auto maxX = (int)ceil(x);
        auto minX = (int)floor(x);
        auto maxY = (int)ceil(y);
        auto minY = (int)floor(y);
        auto tx = x - (int)x;
        auto ty = y - (int)y;

        RGBA32 *rgba = (RGBA32*)mipmaps[mipmapLevel];
        RGBA a = rgba[w * maxY + maxX];
        RGBA b = rgba[w * maxY + minX];
        a = LerpLinear(a, b, tx);

        RGBA c = rgba[w * minY + maxX];
        RGBA d = rgba[w * minY + minX];
        c = LerpLinear(c, d, tx);

        color = LerpLinear(a, c, ty);
    }
    color /= 255.0;
    return color;
}

RGBA Texture2D::GetPixel(int x, int y, const int mipmapLevel)
{
    auto size = GetTextureSize(mipmapLevel);
    int w = size(0), h = size(1);
    switch (warpMode) {
    case WARP_CLAMP:
        x = Clamp(x, 0, w);
        y = Clamp(y, 0, size(1));
        break;
    default:
        auto rx = x % w;
        x = (rx >= 0)? rx : w + rx;
        auto ry = y % h;
        y = (ry >= 0)? ry : h + rx;
        break;
    }

    RGBA rgba = ((RGBA32*)mipmaps[mipmapLevel])[w * y + x];
    rgba /= 255.0;
    return rgba;
}

void Texture2D::SetPixel(const int x, const int y, const RGBA &color, const int mipmapLevel)
{
    auto w = GetTextureSize(mipmapLevel)(0);
    RGBA32 *rgba = (RGBA32*)GetTexturePointer(mipmapLevel);
    rgba[w * y + x] = static_cast<RGBA32>(color * 255.0);
}

Texture2D::Byte *Texture2D::GetTexturePointer(const int mipmapLevel)
{
    return mipmaps[mipmapLevel];
}

Texture2D::Size Texture2D::GetTextureSize(const int mipmapLevel)
{
    return sizes[mipmapLevel];
}

int Texture2D::GetMipmapCount()
{
    return sizes.Count();
}

bool Texture2D::LoadFromFile(Texture2D *&texture, const String &path, const bool multipleMipmaps)
{
    try
    {
        cimg_library::CImg<unsigned char> image(path.c_str());

        texture = new Texture2D();
        {
            Path filePath(path);
            texture->name = filePath.stem().string();
        }

        int w = image.width(), h = image.height(), spectrum = image.spectrum();
        int channelsCount = Min(spectrum, RGBA32::Size()); // channels

        int levels = 0;
        while (true)
        {
            auto data = image.data();
            auto raw = (RGBA32*) (new Byte[w * h * sizeof(RGBA32)]);
            int channelOffset = sizeof(unsigned char) * w * h;

            for (int x = 0; x < w; ++x)
            {
                int _y = 0;
                for (int y = 0; y < h; ++y)
                {
                    int _i = 0;
                    RGBA32* p = &raw[_y + x];
                    for (int i = 0; i < channelsCount; ++i)
                    {
                        (*p)(i) = data[_y + x + _i];
                        _i += channelOffset;
                    }
                    // Fill the remaining channels
                    for (int i = channelsCount; i < RGBA32::Size(); ++i)
                    {
                        (*p)(i) = 255; // Supporsedly this will be only the alpha channel so fill with 255 by default
                    }
                    _y += w;
                }
            }

            texture->mipmaps.Append((Byte*)raw);
            texture->sizes.Append({w, h});

            if (!multipleMipmaps) break;

            levels++;
            if (levels >= 4) break; // FIXME What should be the rule for mipmaps levels creation

            if (w <= 16 || h <= 16) break;

            w = w / 2;
            h = h / 2;

            // Cubic interpolation
            // TODO the result is very odd
            image = image.resize(w, h, 1, spectrum, 5);
        }
    }
    catch(cimg_library::CImgException e) // CImgIOException and CImgArgumentException can be thrown by CImg
    {
        std::cerr << e.what();
        return false;
    }

    return true;
}

void Texture2D::SaveRGBA32TextureAsPNG(const RGBA32 *data, const unsigned width, const unsigned height, const String &path)
{
    cimg_library::CImg<unsigned char> image(width, height, 1, 4);
    for (int x = 0; x < width; ++x)
    {
        for (int y = 0; y < height; ++y)
        {
            for (int i = 0; i < 4; ++i)
            {
                image.atXYZC(x, y, 0, i) = data[width * y + x](i);
            }
        }
    }
    image.save_png(path.c_str());
}
