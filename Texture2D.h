#pragma once

#include <iostream>
#include <CImg.h> // Image library used to load images into the raytracer

#include "Alias.h"
#include "Array.h"
#include "Vector.h"
#include "Util.h"
#include "Lerp.h"
#include "Color.h"

// Avoid templates here
class Texture2D
{    
public:
    typedef Vec<int, 2> Size;
    typedef unsigned char Byte; // used to store bytes

    Array<Byte*> mipmaps;
    Array<Size> sizes;

    // Repeat or clamp (Default behaviour will be Repeat)
    static const int WARP_REPEAT = 0;
    static const int WARP_CLAMP = 1;
    int warpMode = WARP_REPEAT;

    int channels = 4; // For now only supports 4 channels images

    static const int PIXEL_BYTE = sizeof(unsigned char);
    static const int PIXEL_FLOAT = sizeof(float);
    int pixel = PIXEL_BYTE; // Only supports bytes

    String name;

public:
    Texture2D();
    ~Texture2D();

    RGBA SampleBilinear(double x, double y, const int mipmapLevel = 0);

    RGBA GetPixel(int x, int y, const int mipmapLevel = 0);
    void SetPixel(const int x, const int y, const RGBA& color, const int mipmapLevel = 0);

    Byte* GetTexturePointer(const int mipmapLevel = 0);
    Size GetTextureSize(const int mipmapLevel = 0);
    int GetMipmapCount();

    // It loads a RGBA texture from various file types
    static bool LoadFromFile(Texture2D* &texture, const String &path, const bool multipleMipmaps = false);

    static void SaveRGBA32TextureAsPNG(const RGBA32* data, const unsigned width, const unsigned height, const String &path);
};
