#include <cmath> // sin and cossine functions
#include "Transform.h"
#include "Util.h"

Transform::Transform()
	: location({ 0, 0, 0 }), rotation({ 0, 0, 0 }), scale({ 1, 1, 1 })
{
}

Transform::Transform(const Transform & other)
	: location(other.location), rotation(other.rotation), scale(other.scale)
{
}

Transform::Transform(const Vec3 & location, const Vec3 & rotation, const Vec3 & scale)
	: location(location), rotation(rotation), scale(scale)
{
}

const Mat44 Transform::LocalToWorldMatrix()
{
	// TODO improve peformance
	return Translate(location) *
        Rotate(rotation) *
        //(RotateZ(rotation(2)) * RotateY(rotation(1)) * RotateX(rotation(0))) *
		Scale(scale);
}

const Mat44 Transform::WorldToLocalMatrix()
{
    return Scale({ 1.0 / scale(0), 1.0 / scale(1), 1.0 / scale(2) }) *
        Rotate(-rotation) *
        //(RotateX(-rotation(0)) * RotateY(-rotation(1)) * RotateZ(-rotation(2))) *
        Translate(-location);
}

const Vec3 Transform::Forward()
{
    // TODO improve peformance
    return LocalToWorldMatrix() * Vec3{ 0, 0, 1 };
}

const Vec3 Transform::Up()
{
    // TODO improve peformance
    return LocalToWorldMatrix() * Vec3{ 0, 1, 0 };
}

const Vec3 Transform::Right()
{
    // TODO improve peformance
    return LocalToWorldMatrix() * Vec3{ 1, 0, 0 };
}

const Mat44 Transform::Translate(Vec3 location)
{
	Mat44 m = Mat44::Eye();
    for (int i = 0; i < 3; i++) m(3, i) = location(i);
	return m;
}

const Mat44 Transform::Scale(Vec3 scale)
{
	Mat44 m = Mat44::Zero();
    for (int i = 0; i < 3; i++) m(i, i) = scale(i);
	m(3, 3) = 1;
	return m;
}

const Mat44 Transform::RotateX(double x)
{
	double r = x * DEG2RAD;
	double s = sin(r);
    double c = cos(r); //not the same as sqrt(1 - s*s);

	Mat44 b = Mat44::Eye();
	b(1, 1) = c;
	b(2, 2) = c;
	b(1, 2) = s;
	b(2, 1) = -s;

	return b;
}

const Mat44 Transform::RotateY(double y)
{
	double r = y * DEG2RAD;
	double s = sin(r);
    // not the same as sqrt(1 - s*s);
    double c = cos(r);

	Mat44 b = Mat44::Eye();
	b(0, 0) = c;
	b(2, 2) = c;
	b(2, 0) = s;
	b(0, 2) = -s;

	return b;
}

const Mat44 Transform::RotateZ(double z)
{
	double r = z * DEG2RAD;
	double s = sin(r);
    double c = cos(r); // not the same as sqrt(1 - s*s);

	Mat44 b = Mat44::Eye();
	b(0, 0) = c;
	b(1, 1) = c;
	b(0, 1) = s;
	b(1, 0) = -s;

    return b;
}

const Mat44 Transform::Rotate(const Vec3 aa)
{
    // This minus is jut to make sure the result from
    // this Rotate function is consistent with the other overloads
    Vec3 u = aa * -DEG2RAD;
    double angle = u.Lenght();

    if (angle > 1e-6)
    {
        double c = cos(angle);
        double s = sin(angle);
        u = u / angle;

        Mat44 R = Mat44::Zero();
        R(0,0) = c+u(0)*u(0)*(1-c);      R(0,1) = u(0)*u(1)*(1-c)-u(2)*s; R(0,2) = u(0)*u(2)*(1-c)+u(1)*s;
        R(1,0) = u(1)*u(0)*(1-c)+u(2)*s; R(1,1) = c+u(1)*u(1)*(1-c);      R(1,2) = u(1)*u(2)*(1-c)-u(0)*s;
        R(2,0) = u(2)*u(0)*(1-c)-u(1)*s; R(2,1) = u(2)*u(1)*(1-c)+u(0)*s; R(2,2) = c+u(2)*u(2)*(1-c);
        R(3,3) = 1.0;

        return R;
    }

    return Mat44::Eye();
}

const Mat44 Transform::Rotate(double x, double y, double z)
{
    return Transform::Rotate({x, y, z});
}

const Vec3 Transform::Position(Mat44 &matrix)
{
    Vec3 position;
    for (int i = 0; i < 3; ++i)
        position(i) = matrix(3, i);

    return position;
}

const Transform Transform::Identity()
{
	return Transform();
}

/*
Vec3 Transform::getLocation()
{
	return _location;
}

void Transform::setLocation(const Vec3 & location)
{
	dirty = true;
	_location = location;
}

Vec3 Transform::getRotation()
{
	return _rotation;
}

void Transform::setRotation(const Vec3 &rotation)
{
	dirty = true;
	_rotation = rotation;
}

Vec3 Transform::getScale()
{
	return _scale;
}

void Transform::setScale(const Vec3 &scale)
{
	dirty = true;
	_scale = scale;
}
*/
