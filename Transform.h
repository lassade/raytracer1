#pragma once

#include "Matrix.h"
//#include "Property.h"

struct Transform {
	Vec3 location, rotation, scale;

	Transform();
	Transform(const Transform& other);
	Transform(const Vec3& location, const Vec3& rotation, const Vec3& scale);

	const Mat44 LocalToWorldMatrix();
	const Mat44 WorldToLocalMatrix();

    const Vec3 Forward();
    const Vec3 Up();
    const Vec3 Right();

	static const Transform Identity();

	static const Mat44 Translate(Vec3 position);
	static const Mat44 Scale(Vec3 scale);
    static const Mat44 RotateX(double x);
    static const Mat44 RotateY(double y);
    static const Mat44 RotateZ(double z);
    static const Mat44 Rotate(const Vec3 aa);
    static const Mat44 Rotate(double x, double y, double z);

    static const Vec3 Position(Mat44 &matrix);
};
