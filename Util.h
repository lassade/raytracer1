#pragma once

#include <utility>

// useful definitions
const double PI = 3.14159265358979323846264338327950288;
const double DEG2RAD = PI / 180.0;

template<typename T>
inline T Abs(T v)
{
    return (v > 0) ? v : -v;
}

template<typename T>
inline T Max(T v, T o)
{
	return (v > o) ? v : o;;
}

template <typename T>
inline T Max(std::initializer_list<T> argv)
{
	T v = *argv.begin();
	for (auto o : argv)
	{
		v = Max<T>(v, o);
	}
	return v;
}

template<typename T, typename... A>
inline T Max(T v, T o, A... args)
{
	//return Max({ std::forward<T>(v), std::forward<A>(args)... });
	return Max({ v, o, args... });
}

template <typename T>
inline T Max(const T* argv, unsigned size)
{
    T v = *argv;
    for (int i = 0; i < size; ++i)
    {
        v = Max<T>(v, argv[i]);
    }
    return v;
}

template<typename T>
inline T Min(T v, T o)
{
	return (v < o) ? v : o;;
}

template <typename T>
inline T Min(std::initializer_list<T> argv)
{
	T v = *argv.begin();
	for (auto o : argv)
	{
		v = Min<T>(v, o);
	}
	return v;
}

template<typename T, typename... A>
inline T Min(T v, A... args)
{
	//return Max({ std::forward<T>(v), std::forward<A>(args)... });
    return Min<T>({ v, args... });
}

template <typename T>
inline T Min(const T* argv, unsigned size)
{
    T v = *argv;
    for (int i = 0; i < size; ++i)
    {
        v = Min<T>(v, argv[i]);
    }
    return v;
}


template<typename T>
inline T Sum(T v, T o)
{
    return v + o;
}

template <typename T>
inline T Sum(std::initializer_list<T> argv)
{
    T v = 0;
    for (auto o : argv)
    {
        v = Sum<T>(v, o);
    }
    return v;
}

template<typename T, typename... A>
inline T Sum(T v, A... args)
{
    //return Max({ std::forward<T>(v), std::forward<A>(args)... });
    return Sum({ v, args... });
}

template <typename T>
inline T Sum(const T* argv, unsigned size)
{
    T v = 0;
    for (int i = 0; i < size; ++i)
    {
        v = v + argv[i];
    }
    return v;
}


template<typename T>
inline T Clamp(T v, T min, T max)
{
    if (v > max) return max;
    if (v < min) return min;
    return v;
}

