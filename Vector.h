#pragma once

#include <iterator>
#include <cmath>

template<typename T, unsigned N>
struct Vec {
protected:
    T components[N];

public:
	Vec() {}
	Vec(std::initializer_list<T> list)
	{
		unsigned i = 0;
		for (auto &item : list) components[i++] = item;
    }

	// FIXME use parentesis operator
    T& operator ()(const unsigned i) { return components[i]; }
    const T operator ()(const unsigned i) const { return components[i]; }

    operator +=(const Vec<T, N>& v) {
        for (unsigned i = 0; i < N; i++) components[i] += v.components[i];
    }

    operator -=(const Vec<T, N>& v) {
        for (unsigned i = 0; i < N; i++) components[i] -= v.components[i];
    }

    operator *=(const Vec<T, N>& b) {
        for (unsigned i = 0; i < N; i++) components[i] *= b.components[i];
    }

    operator *=(const double scalar) {
        for (unsigned i = 0; i < N; i++) components[i] *= scalar;
    }

    operator /=(const double scalar) {
        double factor = 1.0 / scalar;
        for (unsigned i = 0; i < N; i++) components[i] *= factor;
    }

    static unsigned Size()
    {
        return N;
    }

    T Lenght()
    {
        return (T)sqrt((double)SquaredLenght());
    }
	T SquaredLenght()
	{
        return Dot((*this), (*this));
	}
	void Normalize()
	{
		double s = 1.0 / Lenght();
		for (unsigned i = 0; i < N; i++) components[i] = (T)(components[i] * s);
	}
	Vec<T, N> Normalized()
	{
		return (*this) / Lenght();
	}

    void Set(T element)
    {
        for (unsigned i = 0; i < N; i++) components[i] = element;
    }
    template<typename K>
    void Set(std::initializer_list<K> list)
    {
        unsigned i = 0;
        for (auto &item : list) components[i++] = (T)item;
    }

    template<typename K>
    void Set(K* array)
    {
        for (unsigned i = 0; i < N; i++) components[i] = (T)array[i];
    }

    template<typename K>
    void Copy(K* array)
    {
        for (unsigned i = 0; i < N; i++) array[i] = (K)components[i];
    }

    template<typename K>
    void Get(K* array)
    {
        Copy<K>(array);
    }

    // Convertion to a smaller vector
	template<unsigned M>
	operator Vec<T, M>() const {
		Vec<T, M> v;
        for (int i = 0; i < M; i++) v(i) = (i < N) ? components[i] : 0;
		return v;
	}

    template<typename P, unsigned M>
    operator Vec<P, M>() const {
        return Vec<T, N>::Cast<P, M>(*this);
    }

    // Any type of convertion
    template<typename P, unsigned M>
    static Vec<P, M> Cast(const Vec<T, N> &vec) {
        Vec<P, M> v;
        for (int i = 0; i < M; i++) v(i) = (P) ((i < N) ? vec.components[i] : 0);
        return v;
    }

	// unary negative operator
	Vec<T, N> operator-() const {
		Vec<T, N> v;
        for (int i = 0; i < N; i++) v(i) = -components[i];
		return v;
	}

    static double Dot(const Vec<T, N>& a, const Vec<T, N>& b)
    {
        T v = 0;
        for (unsigned i = 0; i < N; i++) v += (a(i) * b(i));
        return v;
    }
	static Vec<T, N> Zero()
	{
		Vec<T, N> a;
        for (unsigned i = 0; i < N; i++) a(i) = 0;
		return a;
	}
	static Vec<T, N> One()
	{
		Vec<T, N> a;
        for (unsigned i = 0; i < N; i++) a(i) = 1;
		return a;
	}

    //template <typename = typename std::enable_if< (N == 3) >::type >
    const Vec<T, 3> Cross(Vec<T, 3> b)
    {
        Vec<T, 3> r;
        r(0) = components[1] * b(2) - components[2] * b(1);
        r(1) = components[2] * b(0) - components[0] * b(2);
        r(2) = components[0] * b(1) - components[1] * b(0);
        return r;
    }
};

template<typename T, unsigned N>
Vec<T, N> operator +(Vec<T, N> a, const Vec<T, N>& b) {
    for (unsigned i = 0; i < N; i++) a(i) += b(i);
	return a;
}

template<typename T, unsigned N>
Vec<T, N> operator -(Vec<T, N> a, const Vec<T, N>& b) {
    for (unsigned i = 0; i < N; i++) a(i) -= b(i);
	return a;
}

template<typename T, unsigned N>
Vec<T, N> operator* (Vec<T, N> a, const T scalar) {
    for (unsigned i = 0; i < N; i++) a(i) *= scalar;
	return a;
}

template<typename T, unsigned N>
Vec<T, N> operator* (const T scalar, Vec<T, N> a) {
	return a * scalar;
}

template<typename T, unsigned N>
Vec<T, N> operator /(Vec<T, N> a, const T scalar) {
	double s = 1.0 / scalar;
    for (unsigned i = 0; i < N; i++) a(i) *= s;
	return a;
}

template<typename T, unsigned N>
Vec<T, N> operator *(Vec<T, N> a, const Vec<T, N>& b) {
    a *= b;
    return a;
}

typedef Vec<double, 2> Vec2;
typedef Vec<double, 3> Vec3;
typedef Vec<double, 4> Vec4;
